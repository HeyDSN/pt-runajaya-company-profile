<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 Kopiadem.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo $userRootFolder; ?>../lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $userRootFolder; ?>../lte/bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo $userRootFolder; ?>../lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $userRootFolder; ?>../lte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $userRootFolder; ?>../lte/dist/js/app.min.js"></script>
<!-- DataTables -->
<script src="<?php echo $userRootFolder; ?>../lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $userRootFolder; ?>../lte/plugins/datatables/dataTables.bootstrap.min.js"></script>


<!-- Call plugin -->
<script type="text/javascript">
// Delete Button
  function delBtn(del) {
    var answer = confirm("Are you sure wan to Delete?")
    if (answer){
      window.location = del;
    }
  }

  // Autohide element
  $(function(){
    setTimeout(function(){
    $('#autoHideElement').fadeOut('slow');
  }, 3000); // <-- time in milliseconds
  });

  //Delete alert
  $(function () {
    $("#click").click();
  });

    //Data Tables
    $("#data").DataTable({
      "columnDefs": [ {
      "targets": 2,
      "orderable": false
      }]
    });
    
    $('#file').DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": true
    });

</script>
</body>
</html>
