<div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Manage File
          <small>Upload or Delete Your File</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
          <li class="active">Manage File</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      	<div class="row">
	        <div class="col-xs-12">
	            <div class="box box-info">
	                <div class="box-header">
	                <h3 class="box-title">Upload File
	                </h3>
	                <!-- tools box -->
	                <div class="pull-right box-tools">
	                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
	                    <i class="fa fa-plus"></i></button>
	                </div>
	                <!-- /. tools -->
	                </div>
	                <!-- /.box-header -->
	                <div class="box-body pad" >
	                <form enctype="multipart/form-data" action="?action=add-file" method="post">	                
	                <div class="form-group">
	                  <input type="file" name="file" class="form-control" id="file">
	                </div>
	                <div class="form-group">
	                  <input type="submit" name="uploadFile" class="btn btn-primary btn-block" value="Upload File">
	                </div>
	                </form>
	                </div>
	            </div>
	            <!-- /.box -->
	        </div>
     	</div>
      <!-- /.row-->
      	<div class="row">
      		<div class="col-xs-12">
      			<!--box-->
      			<div class="box box-success">
			        <div class="box-header with-border">
			        	<h3 class="box-title">File Lists</h3>
			        </div>
			        <div class="box-body">
			            <table id="data" class="table table-hover table-striped table-bordered">
			                <thead>
			                <tr>
			                  <th style="width: 0.5%;">#</th>
			                  <th>File Name</th>
			                  <th><center><i class="fa fa-gear"></i></center></th>
			                </tr>
			                </thead>
			                <tbody>
			                <form method="POST">
			                 <?php 
			                 	$username = $_SESSION['username'];
				                $db_handle = new DBController();
				                $query ="SELECT * FROM file WHERE username='$username' ORDER BY id_file DESC";
				                $results = $db_handle->selectQuery($query);
				                if($results == 0){}else{
				                $no = 0;
				                foreach($results as $file){ $no++ ?>
			                <tr>	
			                	<td><?php echo $no; ?></td>
			                	<td><?php echo $file['file_name']; ?></td>                  
			                  	<td style="text-align: center;">
			                  		<a href="?action=download&file=<?php echo $file['file']; ?>&filename=<?php echo $file['file_name']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download File</a>
			                  		<a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="delBtn('?action=del-file&id=<?php echo $file['id_file']; ?>&filename=<?php echo $file['file']; ?>')"><i class="fa fa-trash"></i> Delete File</a>
			                  	</td>
			                </tr> 
                			<?php } } ?>    
                			</form>      
			                </tbody>               
			            </table>
			        </div>
		        	<!-- /.box-body -->
		        </div>
       			<!-- /.box -->
      		</div>
      	</div>
       
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
