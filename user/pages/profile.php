<div class="content-wrapper">
	<div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Profile
          <small>Change and Update Your Profile</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="../user-view/"><i class="fa fa-dashboard"></i> User</a></li>
          <li class="active">Profile</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      	<div class="row">
      		<?php 
                $db_handle = new DBController();
                $username = $_SESSION['username'];
                $query ="SELECT * FROM user where username='$username'";
                $results = $db_handle->selectQuery($query);
                foreach ($results as $profile) {
                	$name=$profile['nama'];
                	$email=$profile['email'];
                	$id=$profile['id_user'];
                }
      		 ?>
	        <div class="col-xs-12">
	         	<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <h3><?php echo $name; ?></h3>
              <h5>User</h5>
            </div>
            <form action="?action=edit-profile&id=<?php echo $id; ?>" method="post">
            <div class="box-footer no-padding">
              <ul class="nav">
                <li><br>
                <div class="col-xs-4">Username</div>
                <div class="col-xs-6">
                <div class="form-group">                    
                   	<input class="form-control" value="<?php echo $username ?>" type="text" disabled />
                </div>
                </div></li>
                <li>
                <div class="col-xs-4">Name</div>
                <div class="col-xs-6">
                <div class="form-group">                    
                    <input name="namaUser" class="form-control" value="<?php echo $name; ?>" type="text" required />
                </div>
                </div></li>
                <li>
                <div class="col-xs-4">Email</div>
                <div class="col-xs-6">
                <div class="form-group">                    
                    <input name="emailUser" class="form-control" value="<?php echo $email; ?>" type="text" required />
                </div>
                </div></li>
                <li>
                <div class="col-xs-4">Password</div>
                <div class="col-xs-6">
                <div class="form-group">                    
                    <input name="passwordUser" class="form-control" value="" type="password"/>
                </div>
                </div></li>
                <li>
                <div class="col-xs-12">
                <div class="form-group">                    
                <input type="submit" name="editUser" value="Save Change" class="btn btn-primary btn-block">
                </div>
                </div></li>
              </ul>
            </div>            
            </form>
          </div>  	 
	        </div>
     	</div>
      <!-- /.row-->
      	
       
      </section>
      <!-- /.content -->
    </div>  
  </div>
<?php $statusLoad = true; ?>