<div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Home
          <small>Search and download file you needed</small>
        </h1>

        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
          <li class="active">Home</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">      
          <!-- /.box -->
          <div class="row">
            <div class="col-xs-12">
              <table id="file" class="table table-hover table-striped">
                <thead>
                <tr>
                  <th style="width: 0.5%;">#</th>
                  <th>File Name</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $db_handle = new DBController();
                  $query ="SELECT * FROM file ORDER BY id_file DESC";
                  $results = $db_handle->selectQuery($query);
                  $no = 0;
                  if($results == 0){}else{
                  foreach($results as $file){ $no++?>
                    <tr>  
                      <td><?php echo $no; ?></td>                  
                      <td><?php echo $file['file_name']; ?></td>                  
                      <td style="text-align: right;">
                        <a href="?action=download&file=<?php echo $file['file']; ?>&filename=<?php echo $file['file_name']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download File</a>              
                      </td>
                      </tr> 
                      <?php } } ?>       
                </tbody>               
              </table>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>

