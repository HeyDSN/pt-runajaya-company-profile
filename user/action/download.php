<?php
	$basefolder = "../files/";
	$file = $_GET['file'];
	$ext = explode(".", $file);
	$file = $basefolder.$file;

	$file_name = $_GET['filename'];
	
	if(file_exists($file)){
		header('Content-Description: File Transfer');
		header('Cache-Control: public');
		header('Content-Type: '.$ext);
		header("Content-Transfer-Encoding: binary");
		header('Content-Disposition: attachment; filename='. basename($file_name));
		header('Content-Length: '.filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
	}else{
		echo "File not found!";
	}

?>


