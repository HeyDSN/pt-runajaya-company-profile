<?php
if(isset($_POST['editUser'])){
	$id = $_GET['id'];
	$namaUser = $_POST['namaUser'];
	$emailUser = $_POST['emailUser'];
	$password = $_POST['passwordUser'];
	$level = 0;

	// Default
    $regFail = false;

    // Check email
    $query1 = $mysqli->prepare('SELECT id_user FROM user WHERE email = ? AND id_user != ?');
    $query1->bind_param('si', $emailUser, $id);
    $query1->execute();
    $result1=$query1->get_result();
    $jumlahBaris1=$result1->num_rows;
    if($jumlahBaris1 > 0){
        $regFail = true;
    }

    if($regFail == false){
    	if ($password=="") {
    		// Update profie without password
			$query = $mysqli->prepare('UPDATE user SET nama = ?, email = ? WHERE id_user = ? AND level = ?');
			$query->bind_param('sssi', $namaUser, $emailUser, $id, $level);
			if($query->execute()){
			    $notice = true;
			    $noticeColor = 'bg-green';
			    $noticeMsg = "Data berhasil diupdate.";
			}else{
			    $notice = true;
			    $noticeColor = 'bg-red';
			    $noticeMsg = "Data gagal diupdate.";
			}
    	}else{
			$options = [
				'cost' => 10
			];
			$hash = password_hash($password, PASSWORD_BCRYPT, $options);
			
			// Update profie with password
			$query = $mysqli->prepare('UPDATE user SET nama = ?, email = ?, hash = ? WHERE id_user = ? AND level = ?');
			$query->bind_param('ssssi', $namaUser, $emailUser, $hash, $id, $level);
			if($query->execute()){
			    $notice = true;
			    $noticeColor = 'bg-green';
			    $noticeMsg = "Data berhasil diupdate.";
			}else{
			    $notice = true;
			    $noticeColor = 'bg-red';
			    $noticeMsg = "Data gagal diupdate.";
			}
		}
	}else{
		$notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Data gagal diupdate email telah digunakan";
	}
}
?>
