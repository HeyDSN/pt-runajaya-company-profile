<?php
if(isset($_POST['uploadFile'])){
    // Ambil variabel
    $file_name = $_FILES['file']['name'];
    $upload_date = date('Y-m-d');
    $username = $_SESSION['username'];


    // Upload file
    $errors     = array();
    $maxsize    = 197152;
    $acceptable = array(
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );

    if(($_FILES['file']['size'] >= $maxsize) || ($_FILES["file"]["size"] == 0)) {
        $errors[] = 'File too large, File must be less than 5 megabytes. ';
    }

    if(!in_array($_FILES['file']['type'], $acceptable) && (!empty($_FILES["file"]["type"]))) {
        $errors[] = 'Invalid file type, Only .xls and .xlsx types are accepted.';
    }

    if(count($errors) === 0) {
        $tmp_name = $_FILES["file"]["tmp_name"];
        $ext = explode(".", $_FILES["file"]["name"]);
        $file = round(microtime(true)) . '.' . end($ext);

        // Upload file
        move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$userRootFolder."../files/$file");

        // Insert data to table
        $query = $mysqli->prepare('INSERT INTO file (file, file_name, upload_date, username)values(?,?,?,?)');
        $query->bind_param('ssss', $file, $file_name, $upload_date, $username);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-green';
            $noticeMsg = "file [$file_name] berhasil ditambahkan.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Gallery [$file_name] gagal ditambahkan.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
    
}
?>
