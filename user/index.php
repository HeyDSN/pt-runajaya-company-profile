<?php
// main config
require_once '../includes/autoload.php';

if(!isset($_SESSION['lvl'])) {
    header("Location: $rootFolder/login");
    die;
}else{
    $lvl = $_SESSION['lvl'];
    if ($lvl==1){
      header("Location: $rootFolder/admin");
      die;
    }
}

// set default page
if(empty($_GET['page'])){
    header('Location: user-view');
    die;
}

// save GET to string
$page = $_GET['page'];

if(!file_exists('pages/'.$page.'.php')) {
    header('Location: ../user-view');
    die;
}

if(isset($_GET['action'])){

    // logout action
    if($_GET['action'] == 'logout'){
        $_SESSION['lvl']='';
        $_SESSION['username']='';
        $_SESSION['nama']='';
        session_destroy();
        header('Location:  ' . $_SERVER['PHP_SELF']);
    }
    
    $action = $_GET['action'];
    if (file_exists('action/'.$action.'.php')) {
        // action, please using same name as php file
        include 'action/'.$action.'.php';
    } else {
        header('Location: ../user-view');
        die;
    }
}

// header here
include 'skeleton.header.php';

// navbar here
include 'skeleton.topnavbar.php';

// main content, please using same name as php file
include 'pages/'.$page.'.php';

// footer here
include 'skeleton.footer.php';
?>
