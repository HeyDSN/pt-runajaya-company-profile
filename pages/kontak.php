<section id="contact-info">
        <div class="center">                
            <h2>Kontak Kami</h2>
        </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9825081063154!2d107.07060311505779!3d-6.266030395464642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698fbc1e11a327%3A0xa9cd3cab9bb1941c!2sMetland+Tambun!5e0!3m2!1sid!2sid!4v1502866849096"></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-12">
                                <address>
                                    <h5>ALAMAT KANTOR</h5>
                                    <p>PT. RUNA JAYA TELINDOTAMA <br>
                                    METLAND TAMBUN <br>
                                    Jl.Pirus V Blok J1 No.30 <br>
                                    Tambun - Bekasi <br><br>
                                    <h5>JAM BUKA </h5>
                                    Senin sampai jum'at – 8:00 sampai 14:30 WIB <br>
                                    Phone: +62 21 88 333 3100 <br>
                                    Faxmile : +62 21 88 333 100 <br>
                                    Email: ptrunajaya@gmail.com <br>
                                </address>

                               
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

    <section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Sampaikan Pesan Anda</h2>
                <p class="lead">Kirimkan Pesan Anda Kepada Kami</p>
            </div> 
            <div class="row contact-wrap"> 
                <?php
                if(isset($_POST['sendMessage'])){
                    // Ambil variabel
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $phone = $_POST['phone'];
                    $company = $_POST['company'];
                    $subject = $_POST['subject'];
                    $content = $_POST['message'];

                        // Insert data to table
                        $query = $mysqli->prepare('INSERT INTO messages (name, email, phone, company, subject, content)values(?,?,?,?,?,?)');
                        $query->bind_param('ssssss', $name, $email, $phone, $company, $subject, $content);
                        if($query->execute()){
                            echo '<script>alert("Pesan berhasil dikirim");</script>';
                        }else{
                           echo '<script>alert("Pesan gagal dikirim");</script>';
                        }                        
                    }
                ?>
                <form method="post" action="">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" name="company" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Subject *</label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Message *</label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="sendMessage" class="btn btn-primary btn-lg" required="required">Submit Message</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->