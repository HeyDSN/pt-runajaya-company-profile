<section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Layanan Kami</h2>
                <p class="lead">Layanan Yang Dapat Kami Berikan Kepada Anda</p>
            </div>

            <div class="row">
                <div class="features">
                <?php 
                    $db_handle = new DBController();
                    $query0 ="SELECT * FROM service ORDER BY id ASC ";
                    $results0 = $db_handle->selectQuery($query0);
                    if($results0 == 0){echo '<div class="col-xm-12"><h3>No Data</h3></div</div></div>';}else{
                    foreach($results0 as $service){ ?>
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                    <div class="feature-wrap">
                    <i class="fa <?php echo $service['icon_service']; ?>"></i>
                    <h2><?php echo $service['nama_service']; ?></h2>
                    <h3 style="margin-left:140px;"><?php echo $service['deskripsi_service']; ?></h3>
                    </div>
                    </div>

                    <?php } } ?>
                </div><!--/.services-->
            </div><!--/.row-->        
        </div>

        </div><!--/.container-->
    </section><!--/#feature-->

    <section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Testimoni</h2>
                <p class="lead">Beberapa testimoni dari pelanggan yang telah menggunakan jasa kami</p> 
            </div>

            <div class="row">
            <?php 
                $db_handle = new DBController();
                $query1 ="SELECT * FROM testimoni";
                $results1 = $db_handle->selectQuery($query1);
                if($results1 == 0){echo '<div class="col-xm-12"><h3>No Data</h3></div</div></div>';}else{
                foreach($results1 as $testi){ ?>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="../images/testi-images/<?php echo $testi['foto_testi']; ?>" class="img-circle" alt="">
                        <h3 class="lead"><?php echo $testi['konten_testi']; ?></h3>
                        <h4 class="lead"><span>-<?php echo $testi['nama_testi']; ?> /</span> <?php echo $testi['company_testi']; ?></h4>
                    </div>
                </div>
            <?php } } ?>
           </div>
        </div><!--/.container-->
    </section><!--/#services-->
