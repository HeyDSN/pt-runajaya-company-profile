<section id="product">
        <div class="container">
        <div class="row" id="aniimated-thumbnials">
            <div class="center">
               <h2>Produk</h2>
               <p class="lead">Daftar produk yang kami gunakan</p>
            </div>
            <?php 
                $db_handle = new DBController();
                $query ="SELECT * FROM product ORDER BY id ASC ";
                $results = $db_handle->selectQuery($query);
                if($results == 0){echo '<div class="col-xm-12"><h3>No Data</h3></div</div></div>';}else{
                foreach($results as $product){ ?>
                   <div class="col-md-12 col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                       <div class="feature-wrap">
                            <div class="col-xs-3">
                            <a href="../images/product/<?php echo $product['gambar_product']; ?>" data-sub-html="<?php echo $product['nama_product']; ?>" class="galimg">
                            <img src="../images/product/<?php echo $product['gambar_product']; ?>" class="img-responsive img-thumbnail" alt="<?php echo $product['nama_product']; ?>">
                            </a>
                            </div>
                            <div class="col-xs-9">
                            <h2><?php echo $product['nama_product']; ?></h2><hr>
                            <h3><?php echo $product['deskripsi_product']; ?></h3>    
                            </div>                            
                        </div>
                    </div><!--/.col-md-4-->
            <?php } } ?>
        </div>
        </div>
</section>