    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <div class="carousel-inner">
                <?php 
                $db_handle = new DBController();
        		$query0 ="SELECT * FROM slide_show ";
        		$results0 = $db_handle->selectQuery($query0);
        		if($results0 == 0){echo '<div class="center"><h2>No Data</h2></div>';}else{
        		foreach($results0 as $slide){ $urutan=$slide['urutan']; ?>
                        <div class="item <?php if ($urutan==1){echo "active";}?>" style="background-image: url(../images/slide-show/<?php echo $slide['gambar']; ?>)">
                            <div class="container">
                                <div class="row slide-margin">
                                    <div class="col-sm-6">
                                        <div class="carousel-content">
                                            <h1 class="animation animated-item-1"><?php echo $slide['judul']; ?></h1>
                                            <h2 class="animation animated-item-2"><?php echo $slide['deskripsi']; ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/.item-->
                <?php } } ?>        
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->


    <section id="recent-works">
    <?php 
		$db_handle = new DBController();
		$query1 ="SELECT tentang FROM tentang_perusahaan";
		$results1 = $db_handle->selectQuery($query1);
		foreach($results1 as $tentang){ 
			$tentang=$tentang['tentang'];
		}
	?>
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Tentang Perusahaan</h2>
                <p class="lead">Penjalasan Singkat Perusahaan</p>
            </div>
            <div class="row">
                 <div class="col-md-3">
                     <img class="img-responsive" src="../images/logo/logo-runa-jaya.png">
                 </div>
                 <div class="col-md-9" style="text-align: justify; text-justify: inter-word;">
                 	<?php echo $tentang; ?>
                 </div>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#recent-works-->

    <section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Layanan Yang Kami Tawarkan</h2>
            </div>
            <div class="row">
            	<?php 
            		$db_handle = new DBController();
				    $query2 ="SELECT * FROM service ORDER BY id ASC LIMIT 3";
				    $results2 = $db_handle->selectQuery($query2);
				    if($results2 == 0){echo '<div class="col-xm-12"> <div class="media services-wrap wow fadeInDown"><div class="media-body"><h3>No Data</h3></div</div></div>';}else{
				    foreach($results2 as $service){ ?>
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <i class="fa <?php echo $service['icon_service']; ?> fa-5x"></i>
                        </div>
                        <div class="media-body">
                            <a href="../layanan/"><h1 class="media-heading" style="color: #000;margin-top: 12px;"><?php echo $service['nama_service']; ?></h1></a>
                        </div>
                    </div>
                </div>                        
                <?php } } ?>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->

    <section id="middle">
        <div class="container">
         <div class="center wow fadeInDown">
            <h2>Aktivitas Terakhir Kami</h2>
        </div>
            <div class="row">
                <div class="col-sm-12 wow fadeInDown"> 
                <?php 
            		$db_handle = new DBController();
				    $query3 ="SELECT * FROM gallery ORDER BY no DESC LIMIT 4";
				    $results3 = $db_handle->selectQuery($query3);
				    if($results3 == 0){echo '<div class="center wow fadeInDown"><h2>No Data</h2></div>';}else{
				    foreach($results3 as $gallery){ ?>               
                    <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive gallery" src="../images/image-gallery/<?php echo $gallery['gambar']; ?>" alt="">
                        <div class="overlay" style="display:none;">
                            <div class="recent-work-inner">
                                <h2 style="text-align: center; color: white;"><?php echo $gallery['judul']; ?></h2>
                                <p class="center"><a class="preview" href="../images/image-gallery/<?php echo $gallery['gambar']; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a></p>
                            </div> 
                        </div>
                    </div>
                	</div>
                <?php } } ?>	 
                </div><!--/.col-sm-6-->               
            </div><!--/.row-->
            <div class="row">
                <h2 class="center" style="padding-bottom:0px;"><a href="../galeri">View More</i></a></h2>
            </div>
        </div><!--/.container-->
    </section><!--/#middle-->
    <section id="partner">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Partner Kami</h2>                
            </div>  
            <div class="partners">
                <div class="owl-carousel owl-theme">
            <div class="item">
              <img src="../images/partners/samsung.png">
            </div>
            <div class="item">
              <img src="../images/partners/Legrand.png">
            </div>
            <div class="item">
              <img src="../images/partners/intisumber.png">           
            </div>
            <div class="item">
              <img src="../images/partners/citra_karsa.png">           
            </div>
            <div class="item">
              <img src="../images/partners/elsiscom.jpeg">           
            </div>
            </div>        
        </div><!--/.container-->
    </section><!--/#partner-->

    <section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="pull-left">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h2>Anda memiliki pertanyaan atau hal yang membuat anda bingung ?</h2>
                            <p>Anda dapat menghubungi kami melalui nomer tlp +62 21 88 333 3100</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->