<section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Galeri</h2>
               <p class="lead">Dokumentasi Kegiatan Perusahaan</p>
            </div>
        	
             <div class="row">
            <div class="col-xs-12">
            <div id="aniimated-thumbnials" class="list-unstyled row clearfix" style="text-align: center;">
                <?php 
                $db_handle = new DBController();
                $query ="SELECT * FROM gallery ORDER BY no ASC";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <h3>Gallery Empty!</h3>
                </div>
                <?php }else{
                $no = 0;
                foreach($results as $gallery){ ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">                
                    <a href="<?php echo $adminRootFolder; ?>../images/image-gallery/<?php echo $gallery['gambar']; ?>" data-sub-html="<?php echo $gallery['judul']; ?>" class="galimg">
                        <img class="img-responsive thumbnail gallery" src="<?php echo $adminRootFolder; ?>../images/image-gallery/<?php echo $gallery['gambar']; ?>">

                    </a>
                </div>
                <?php } } ?>
            </div>
        </div>
      </div>
      <!-- /.row -->
        </div>
    </section><!--/#portfolio-item-->

