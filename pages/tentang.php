<?php 
$db_handle = new DBController();
$query0 ="SELECT * FROM tentang_perusahaan ";
$results0 = $db_handle->selectQuery($query0);
if($results0 == 0){ }else{
foreach($results0 as $tp){ 
	$tentang=$tp['tentang'];
	$visi=$tp['visi'];
	$misi=$tp['misi'];
	$struktur=$tp['struktur'];		     
} } 
?>

<section id="about-us">
    <div class="container">
    	
            <div class="row">
            	<div class="col-md-12">
            		<img class="img-responsive" src="../images/<?php echo $struktur; ?>" style="max-width: 100%;height: auto;display: block;margin: 0 auto;">
            	</div>
            </div>
	</div><!--/.container-->
</section><!--/about-us-->
<section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Visi & Misi</h2>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12" style="background: rgba(255, 255, 255, 1);">
                    <div class="col-sm-6 col-md-6">
                        <div class="media services-wrap wow fadeInDown" style="background: rgba(255, 255, 255, 0);">
                            <div class="pull-left">
                                <i class="fa fa-binoculars fa-5x"></i>
                            </div>
                            <div class="media-body">
                            	<h1 class="media-heading" style="color: #000;margin-top: 12px;">Visi</h1><hr>
                            	<?php echo $visi; ?>
                            </div>
                        </div>
                    </div>  
                    <div class="col-sm-6 col-md-6">
                        <div class="media services-wrap wow fadeInDown" style="background: rgba(255, 255, 255, 0);">
                            <div class="pull-left">
                                <i class="fa fa-rocket fa-5x"></i>
                            </div>
                            <div class="media-body">
                            	<h1 class="media-heading" style="color: #000;margin-top: 12px;">Misi</h1><hr>
                            	<?php echo $misi; ?>
                            </div>
                        </div>
                    </div>
                </div><!--/.col-->   
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->
	