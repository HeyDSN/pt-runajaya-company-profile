<body class="homepage">
    <header id="header">
    <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +62 21 88 333 3100</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../home/" style="margin-bottom: 5px;"><img width="50px" class="img-responsive" src="../images/logo/logo-runa-jaya.png"> </a>
                </div>				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="<?php if($page=="home"){echo "active";}?>"><a href="../home/">Home</a></li>
                        <li class="<?php if($page=="tentang"){echo "active";}?>"><a href="../tentang/">Tentang</a></li>
                        <li class="<?php if($page=="layanan"){echo "active";}?>"><a href="../layanan/">Layanan</a></li>
                        <li class="<?php if($page=="produk"){echo "active";}?>"><a href="../produk/">Produk</a></li>
                        <li class="<?php if($page=="galeri"){echo "active";}?>"><a href="../galeri/">Galeri</a></li> 
                        <li class="<?php if($page=="kontak"){echo "active";}?>"><a href="../kontak/">Kontak Kami</a></li>                
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->		
    </header><!--/header-->

