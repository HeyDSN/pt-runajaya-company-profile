<?php
// Root folder same as htaccess
$rootFolder = '/KopiAdemProjects';
$adminRootFolder = '/KopiAdemProjects/admin/';
$userRootFolder = '/KopiAdemProjects/user/';

// Config Database
include($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../includes/config.db.php");

// Config General
include($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../includes/config.general.php");

// Function
include($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../includes/func.php");

session_start()
?>
