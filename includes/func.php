<?php
// Fungsi database
class DBController {
	private $hostDB;
    private $userDB;
    private $passDB;
    private $nameDB;

    function __construct() {
        include("config.db.php");
        $this->hostDB = $hostDB;
        $this->userDB = $userDB;
        $this->passDB = $passDB;
        $this->nameDB = $nameDB;
    }

	function runQuery($query) {
		$conn = mysqli_connect($this->hostDB,$this->userDB,$this->passDB,$this->nameDB);
		$run = mysqli_query($conn,$query);
	}

	function selectQuery($query) {
		$conn = mysqli_connect($this->hostDB,$this->userDB,$this->passDB,$this->nameDB);
		$result = mysqli_query($conn,$query);
		$rows = mysqli_num_rows($result);
		if($rows > 0){
			while($row=mysqli_fetch_assoc($result)){
				$resultset[] = $row;
			}
			return $resultset;
		}else{
			return $rows;
		}
	}

	function countQuery($query) {
		$conn = mysqli_connect($this->hostDB,$this->userDB,$this->passDB,$this->nameDB);
		$result = mysqli_query($conn,$query);
		$rows = mysqli_fetch_row($result);
		return $rows[0];
	}
}

// Fungsi buat random key untuk ID unik berdasarkan microtime dan random string
function randomKey(){
	$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	$max = strlen($characters) - 1;
	for ($i = 0; $i < 3; $i++) {
		$string .= $characters[mt_rand(0, $max)];
	}
	$key = uniqid($string);
	return $key;
}

// Fungsi Format tangga ke format Indonesia ("Y-m-d")
function formatIndonesia($tanggal, $cetak_hari = false)
{
	$hari = array( 1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu' );
	$bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' );
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}

?>
