<?php
date_default_timezone_set('Asia/Jakarta');
$hostDB = "localhost";
$nameDB = "proj_one";
$userDB = "root";
$passDB = "";

$mysqli = new mysqli($hostDB, $userDB, $passDB, $nameDB);

if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    die();
}
?>
