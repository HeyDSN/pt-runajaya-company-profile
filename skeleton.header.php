<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo ucfirst($page); ?> | Runa Jaya</title>
	
	<!-- core CSS -->
    <link href="<?php echo $rootFolder; ?>/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/main.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/responsive.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/owl.theme.default.css" rel="stylesheet">
    <link href="<?php echo $rootFolder; ?>/dist/css/owl.carousel.css" rel="stylesheet">
    <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php if($page == "galeri" or $page == "produk" or $page == "home"){ ?>
      <!-- Lightgallery Plugin -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/lightgallery/1.3.9/css/lightgallery.css">
      <style>
        img.gallery{
            object-fit: cover;
            width: 260px;
            height: 146.250px;
            display: block;
            margin: auto;
            margin-bottom: 15px;
        }
      </style>
    <?php } ?>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../images/ico/logo.ico">
</head><!--/head-->