<?php
if(isset($_POST['editProduct'])){
    // Ambil variabel
    $id = $_GET['id'];
    $namaGambarProduct = $_POST['namaGambarProduct'];
    $namaProduct = $_POST['namaProduct'];
    $deskripsiProduct = $_POST['deskripsiProduct'];

    if($_FILES['gambarProduct']['name'] !== ""){
        // Upload pictures
        $errors     = array();
        $maxsize    = 2097152;
        $acceptable = array(
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png'
        );

        if(($_FILES['gambarProduct']['size'] >= $maxsize) || ($_FILES["gambarProduct"]["size"] == 0)) {
            $errors[] = 'File too large. File must be less than 2 megabytes. ';
        }

        if(!in_array($_FILES['gambarProduct']['type'], $acceptable) && (!empty($_FILES["gambarProduct"]["type"]))) {
            $errors[] = 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
        }

        $noUpload = false;
    }else{
        $errors = array();
        $noUpload = true;
    }

    if(count($errors) === 0) {
        if($noUpload == false){
            $ext = explode(".", $_FILES["gambarProduct"]["name"]);
            $newNamaGambarProduct = round(microtime(true)) . '.' . end($ext);
            $tmp_name = $_FILES["gambarProduct"]["tmp_name"];

            // Delete old files
            if(file_exists($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$namaGambarProduct")){
                unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$namaGambarProduct");
            }

            // Upload file
            move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$newNamaGambarProduct");
        }

        // Insert data to table
        $query = $mysqli->prepare('UPDATE product SET gambar_product = ?, nama_product = ?, deskripsi_product = ? WHERE id = ?');
        $query->bind_param('ssss', $newNamaGambarProduct, $namaProduct, $deskripsiProduct, $id);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-blue';
            $noticeMsg = "Product [$namaProduct] berhasil diubah.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Product [$namaProduct] gagal diubah.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
