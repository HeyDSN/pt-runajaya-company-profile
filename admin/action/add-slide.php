<?php
if(isset($_POST['tambahSlide'])){
    // Ambil variabel
    $judulSlide = $_POST['judulSlide'];
    $deskripsiSlide = $_POST['deskripsiSlide'];

    // Upload pictures
    $errors     = array();
    $maxsize    = 2097152;
    $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    );

    if(($_FILES['gambarSlide']['size'] >= $maxsize) || ($_FILES["gambarSlide"]["size"] == 0)) {
        $errors[] = 'File too large. File must be less than 2 megabytes. ';
    }

    if(!in_array($_FILES['gambarSlide']['type'], $acceptable) && (!empty($_FILES["gambarSlide"]["type"]))) {
        $errors[] = 'Invalid file type. Only PDF, JPG, GIF and PNG types are accepted.';
    }

    if(count($errors) === 0) {
        $tmp_name = $_FILES["gambarSlide"]["tmp_name"];
        $ext = explode(".", $_FILES["gambarSlide"]["name"]);
        $gambarSlide = round(microtime(true)) . '.' . end($ext);

        // Upload file
        move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/slide-show/$gambarSlide");

        // Insert data to table
        $query = $mysqli->prepare('INSERT INTO slide_show (judul, deskripsi, gambar)values(?, ?, ?)');
        $query->bind_param('sss', $judulSlide, $deskripsiSlide, $gambarSlide);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-green';
            $noticeMsg = "Slide show [$judulSlide] berhasil ditambah.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Slide show [$judulSlide] gagal ditambah.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
