<?php
if(isset($_POST['ubahSlide'])){
	$no = $_GET['id'];
	$judul = $_POST['judulSlide'];
	$deskripsi = $_POST['deskripsiSlide'];
	$query = $mysqli->prepare('UPDATE slide_show SET judul = ?, deskripsi = ? WHERE no = ?');
	$query->bind_param('sss', $judul, $deskripsi, $no);
	if($query->execute()){
		$affected = $query->affected_rows;
		if($affected >= 1){
		    $notice = true;
		    $noticeColor = 'bg-blue';
		    $noticeMsg = "Slide show berhasil diedit.";
		}
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Slide show gagal diedit.";
	}
}
?>
