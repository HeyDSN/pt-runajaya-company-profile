<?php
if(isset($_POST['editStruktur'])){
    // Ambil variabel
    $fileName = $_POST['fileName'];

    if($_FILES['file']['name'] !== ""){
        // Upload pictures
        $errors     = array();
        $maxsize    = 2097152;
        $acceptable = array(
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png'
        );

        if(($_FILES['file']['size'] >= $maxsize) || ($_FILES["file"]["size"] == 0)) {
            $errors[] = 'File too large. File must be less than 2 megabytes. ';
        }

        if(!in_array($_FILES['file']['type'], $acceptable) && (!empty($_FILES["file"]["type"]))) {
            $errors[] = 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
        }

        $noUpload = false;
    }else{
        $errors = array();
        $noUpload = true;
    }

    if(count($errors) === 0) {
        if($noUpload == false){
            $ext = explode(".", $_FILES["file"]["name"]);
            $newFileName = round(microtime(true)) . '.' . end($ext);
            $tmp_name = $_FILES["file"]["tmp_name"];

            // Delete old files
            if(file_exists($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/$fileName")){
                unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/$fileName");
            }

            // Upload file
            move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/$newFileName");
        }

        // Insert data to table
        $id = 1;
        $query = $mysqli->prepare('UPDATE tentang_perusahaan SET struktur = ? WHERE id = ?');
        $query->bind_param('ss', $newFileName, $id);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-blue';
            $noticeMsg = "Struktur perusahaan berhasil diubah.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Struktur perusahaan gagal diubah.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
