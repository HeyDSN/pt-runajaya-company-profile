<?php
if(isset($_POST['editUser'])){
	$id = $_GET['id'];
	$namaUser = $_POST['namaUser'];
	$emailUser = $_POST['emailUser'];
	$usernameUser = $_POST['usernameUser'];

	//Old data
	$oldEmailUser = $_POST['oldEmailUser'];
	$oldUsernameUser = $_POST['oldUsernameUser'];

	// Default
    $regFail = false;

    if($usernameUser != $oldUsernameUser){
		// Check username
	    $query1 = $mysqli->prepare('SELECT id_user FROM user WHERE username = ?');
	    $query1->bind_param('s', $usernameUser);
	    $query1->execute();
	    $result1=$query1->get_result();
	    $jumlahBaris1=$result1->num_rows;
	    if($jumlahBaris1 > 0){
	        $regFail = true;
	        $regStatMess = "Username";
	    }
	}

	if($emailUser != $oldEmailUser){
	    // Check email
	    $query2 = $mysqli->prepare('SELECT id_user FROM user WHERE email = ?');
	    $query2->bind_param('s', $emailUser);
	    $query2->execute();
	    $result2=$query2->get_result();
	    $jumlahBaris2=$result2->num_rows;
	    if($jumlahBaris2 > 0){
	        $regFail = true;
	        if(isset($regStatMess)){
	            $regStatMess = "Username & Email";
	        }else{
	            $regStatMess = "Email";
	        }
	    }
	}

    if($regFail == false){
		$password = $_POST['passwordUser'];
		$options = [
			'cost' => 10
		];
		$hash = password_hash($password, PASSWORD_BCRYPT, $options);
		$level = 0;

		// Add user
		$query = $mysqli->prepare('UPDATE user SET nama = ?, email = ?, username = ?, hash = ? WHERE id_user = ? AND level = ?');
		$query->bind_param('sssssi', $namaUser, $emailUser, $usernameUser, $hash, $id, $level);
		if($query->execute()){
		    $notice = true;
		    $noticeColor = 'bg-green';
		    $noticeMsg = "Data user [$usernameUser - $namaUser] berhasil diupdate.";
		}else{
		    $notice = true;
		    $noticeColor = 'bg-red';
		    $noticeMsg = "Data user [$usernameUser - $namaUser] gagal diupdate.";
		}
	}else{
		$notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "User [$usernameUser - $namaUser] gagal diupdate. Data berikut telah digunakan: $regStatMess";
	}
}
?>
