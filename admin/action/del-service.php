<?php
    $id = $_GET['id'];
    // Delete data from tabel
    $query=$mysqli->prepare("DELETE FROM service WHERE id=?");
    $query->bind_param('s', $id);
    if($query->execute()){
	    $affected = $query->affected_rows;
	    if($affected >= 1){
	        $notice = true;
	        $noticeColor = 'bg-yellow';
	        $noticeMsg = "Service berhasil dihapus.";
	    }
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Service gagal dihapus.";
	}
?>
