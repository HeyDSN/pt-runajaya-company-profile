<?php
if(isset($_POST['tentang'])){
	$id = 1;
	$tentang = $_POST['tentang'];
	$query = $mysqli->prepare('UPDATE tentang_perusahaan SET tentang = ? WHERE id = ?');
	$query->bind_param('ss', $tentang, $id);
	if($query->execute()){
	    $notice = true;
	    $noticeColor = 'bg-green';
	    $noticeMsg = "Tentang perusahaan berhasil diubah.";
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Tentang perusahaan gagal diubah.";
	}
}
?>
