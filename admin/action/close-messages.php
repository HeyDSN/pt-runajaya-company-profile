<?php
    $id = $_GET['id'];
    $status = 1;
    $query = $mysqli->prepare('UPDATE messages SET status = ? WHERE id = ?');
    $query->bind_param('ss', $status, $id);
    if($query->execute()){
    $affected = $query->affected_rows;
		if($affected >= 1){
		    $notice = true;
		    $noticeColor = 'bg-green';
		    $noticeMsg = "Status pesan berhasil diubah.";
		}
	}else{
		$notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Status pesan gagal diubah.";
	}
?>
