<?php
if(isset($_POST['addProduct'])){
    // Ambil variabel
    $namaProduct = $_POST['namaProduct'];
    $deskripsiProduct = $_POST['deskripsiProduct'];

    // Upload pictures
    $errors     = array();
    $maxsize    = 2097152;
    $acceptable = array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    );

    if(($_FILES['gambarProduct']['size'] >= $maxsize) || ($_FILES["gambarProduct"]["size"] == 0)) {
        $errors[] = 'File too large. File must be less than 2 megabytes. ';
    }

    if(!in_array($_FILES['gambarProduct']['type'], $acceptable) && (!empty($_FILES["gambarProduct"]["type"]))) {
        $errors[] = 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
    }

    if(count($errors) === 0) {
        $tmp_name = $_FILES["gambarProduct"]["tmp_name"];
        $ext = explode(".", $_FILES["gambarProduct"]["name"]);
        $gambarProduct = round(microtime(true)) . '.' . end($ext);

        // // If file already exist, delete
        // if(file_exists($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$gambarProduct")){
        //     unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$gambarProduct");
        // }

        // Upload file
        move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$gambarProduct");

        // Insert data to table
        $query = $mysqli->prepare('INSERT INTO product (gambar_product, nama_product, deskripsi_product)values(?, ?, ?)');
        $query->bind_param('sss', $gambarProduct, $namaProduct, $deskripsiProduct);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-green';
            $noticeMsg = "Product [$namaProduct] berhasil ditambahkan.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Product [$namaProduct] gagal ditambahkan.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
