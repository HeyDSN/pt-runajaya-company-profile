<?php
if(isset($_POST['editTesti'])){
    // Ambil variabel
    $id = $_GET['id'];
    $namaFotoTesti = $_POST['namaFotoTesti'];
    $namaTesti = $_POST['namaTesti'];
    $companyTesti = $_POST['companyTesti'];
    $kontenTesti = $_POST['kontenTesti'];

    if($_FILES['fotoTesti']['name'] !== ""){
        // Upload pictures
        $errors     = array();
        $maxsize    = 2097152;
        $acceptable = array(
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png'
        );

        if(($_FILES['fotoTesti']['size'] >= $maxsize) || ($_FILES["fotoTesti"]["size"] == 0)) {
            $errors[] = 'File too large. File must be less than 2 megabytes. ';
        }

        if(!in_array($_FILES['fotoTesti']['type'], $acceptable) && (!empty($_FILES["fotoTesti"]["type"]))) {
            $errors[] = 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
        }

        $noUpload = false;
    }else{
        $errors = array();
        $noUpload = true;
    }

    if(count($errors) === 0) {
        if($noUpload == false){
            $tmp_name = $_FILES["fotoTesti"]["tmp_name"];

            // Delete old files
            if(file_exists($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/testi-images/$namaFotoTesti")){
                unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/testi-images/$namaFotoTesti");
            }

            // Upload file
            move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/testi-images/$namaFotoTesti");
        }

        // Insert data to table
        $query = $mysqli->prepare('UPDATE testimoni SET foto_testi = ?, nama_testi = ?, company_testi = ?, konten_testi =? WHERE id_testi = ?');
        $query->bind_param('sssss', $namaFotoTesti, $namaTesti, $companyTesti, $kontenTesti, $id);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-blue';
            $noticeMsg = "Data testimoni berhasil diubah.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Data testimoni gagal diubah.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
