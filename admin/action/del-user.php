<?php
$id = $_GET['id'];
$level = 0;
// Insert data to table
$query=$mysqli->prepare("DELETE FROM user WHERE id_user=? AND level = ?");
$query->bind_param('si', $id, $level);
if($query->execute()){
    $affected = $query->affected_rows;
    if($affected >= 1){
        $notice = true;
        $noticeColor = 'bg-yellow';
        $noticeMsg = "Data user berhasil dihapus.";
    }
}else{
    $notice = true;
    $noticeColor = 'bg-red';
    $noticeMsg = "Data user gagal dihapus.";
}
?>
