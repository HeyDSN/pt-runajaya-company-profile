<?php
if(isset($_POST['tambahGallery'])){
    // Ambil variabel
    $judulGambar = $_POST['judulGambar'];

    // Upload pictures
    $errors     = array();
    $maxsize    = 2097152;
    $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    );

    if(($_FILES['gambarGallery']['size'] >= $maxsize) || ($_FILES["gambarGallery"]["size"] == 0)) {
        $errors[] = 'File too large. File must be less than 2 megabytes. ';
    }

    if(!in_array($_FILES['gambarGallery']['type'], $acceptable) && (!empty($_FILES["gambarGallery"]["type"]))) {
        $errors[] = 'Invalid file type. Only PDF, JPG, GIF and PNG types are accepted.';
    }

    if(count($errors) === 0) {
        $tmp_name = $_FILES["gambarGallery"]["tmp_name"];
        $ext = explode(".", $_FILES["gambarGallery"]["name"]);
        $gambarGallery = round(microtime(true)) . '.' . end($ext);

        // Upload file
        move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/image-gallery/$gambarGallery");

        // Insert data to table
        $query = $mysqli->prepare('INSERT INTO gallery (judul, gambar)values(?, ?)');
        $query->bind_param('ss', $judulGambar, $gambarGallery);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-green';
            $noticeMsg = "Gallery [$judulGambar] berhasil ditambahkan.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Gallery [$judulGambar] gagal ditambahkan.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
