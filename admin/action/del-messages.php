<?php
    $id = $_GET['id'];
    // Insert data to table
    $query=$mysqli->prepare("DELETE FROM messages WHERE id=?");
    $query->bind_param('s', $id);
    if($query->execute()){
    $affected = $query->affected_rows;
		if($affected >= 1){
		    $notice = true;
		    $noticeColor = 'bg-yellow';
		    $noticeMsg = "Pesan berhasil dihapus.";
		}
	}else{
		$notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Pesan gagal dihapus.";
	}
?>
