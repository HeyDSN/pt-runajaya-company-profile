<?php
if(isset($_POST['misi'])){
	$id = 1;
	$misi = $_POST['misi'];
	$query = $mysqli->prepare('UPDATE tentang_perusahaan SET misi = ? WHERE id = ?');
	$query->bind_param('ss', $misi, $id);
	if($query->execute()){
	    $notice = true;
	    $noticeColor = 'bg-green';
	    $noticeMsg = "Misi perusahaan berhasil diubah.";
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Misi perusahaan gagal diubah.";
	}
}
?>
