<?php
if(isset($_POST['editService'])){
	$id = $_GET['id'];
	$iconService = $_POST['iconService'];
	$namaService = $_POST['namaService'];
	$deskripsiService = $_POST['deskripsiService'];

	// Edit service
	$query = $mysqli->prepare('UPDATE service SET icon_service = ?, nama_service = ?, deskripsi_service = ? WHERE id = ?');
	$query->bind_param('ssss', $iconService, $namaService, $deskripsiService, $id);
	if($query->execute()){
	    $affected = $query->affected_rows;
	    if($affected >= 1){
	        $notice = true;
	        $noticeColor = 'bg-blue';
	        $noticeMsg = "Service berhasil diubah.";
	    }
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Service gagal diubah.";
	}
}
?>
