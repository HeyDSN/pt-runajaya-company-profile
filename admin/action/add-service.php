<?php
if(isset($_POST['addService'])){
	$iconService = $_POST['iconService'];
	$namaService = $_POST['namaService'];
	$deskripsiService = $_POST['deskripsiService'];

	// Add service
	$query = $mysqli->prepare('INSERT INTO service (icon_service, nama_service, deskripsi_service)values(?, ?, ?)');
	$query->bind_param('sss', $iconService, $namaService, $deskripsiService);
	if($query->execute()){
	    $notice = true;
	    $noticeColor = 'bg-green';
	    $noticeMsg = 'Service [<i class="fa '.$iconService.'"></i> '.$namaService.'] berhasil ditambah.';
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = 'Service [<i class="fa '.$iconService.'"></i> '.$namaService.'] gagal ditambah.';
	}
}
?>
