<?php
$id = $_GET['id'];
$filename = $_GET['filename'];
// Insert data to table
$query=$mysqli->prepare("DELETE FROM testimoni WHERE id_testi=?");
$query->bind_param('s', $id);
if($query->execute()){
    $affected = $query->affected_rows;
    if($affected >= 1){
        unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/testi-images/$filename");
        $notice = true;
        $noticeColor = 'bg-yellow';
        $noticeMsg = "Data testimoni berhasil dihapus.";
    }
}else{
    $notice = true;
    $noticeColor = 'bg-red';
    $noticeMsg = "Data testimoni gagal dihapus.";
}
?>
