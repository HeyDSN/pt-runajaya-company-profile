<?php
if(isset($_POST['addTesti'])){
    // Ambil variabel
    $namaTesti = $_POST['namaTesti'];
    $companyTesti = $_POST['companyTesti'];
    $kontenTesti = $_POST['kontenTesti'];

    // Upload pictures
    $errors     = array();
    $maxsize    = 2097152;
    $acceptable = array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    );

    if(($_FILES['fotoTesti']['size'] >= $maxsize) || ($_FILES["fotoTesti"]["size"] == 0)) {
        $errors[] = 'File too large. File must be less than 2 megabytes. ';
    }

    if(!in_array($_FILES['fotoTesti']['type'], $acceptable) && (!empty($_FILES["fotoTesti"]["type"]))) {
        $errors[] = 'Invalid file type. Only JPG, GIF and PNG types are accepted.';
    }

    if(count($errors) === 0) {
        $tmp_name = $_FILES["fotoTesti"]["tmp_name"];
        $ext = explode(".", $_FILES["fotoTesti"]["name"]);
        $fotoTesti = round(microtime(true)) . '.' . end($ext);

        // // If file already exist, delete
        // if(file_exists($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$gambarProduct")){
        //     unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$gambarProduct");
        // }

        // Upload file
        move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/testi-images/$fotoTesti");

        // Insert data to table
        $query = $mysqli->prepare('INSERT INTO testimoni (foto_testi, nama_testi, company_testi, konten_testi)values(?, ?, ?, ?)');
        $query->bind_param('ssss', $fotoTesti, $namaTesti, $companyTesti, $kontenTesti);
        if($query->execute()){
            $notice = true;
            $noticeColor = 'bg-green';
            $noticeMsg = "Data testimoni baru berhasil ditambahkan.";
        }else{
            $notice = true;
            $noticeColor = 'bg-red';
            $noticeMsg = "Data testimoni gagal ditambahkan.";
        }
    } else {
        $noticeMsg = "Error: ";
        foreach($errors as $error) {
            $noticeMsg .= $error;
        }
        $notice = true;
        $noticeColor = 'bg-red';
    }
}
?>
