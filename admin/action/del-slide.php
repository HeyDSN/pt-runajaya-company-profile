<?php
$id = $_GET['id'];
$filename = $_GET['filename'];
// Insert data to table
$query=$mysqli->prepare("DELETE FROM slide_show WHERE no=?");
$query->bind_param('s', $id);
if($query->execute()){
	$affected = $query->affected_rows;
	if($affected >= 1){
	    unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/slide-show/$filename");
	    $notice = true;
	    $noticeColor = 'bg-yellow';
	    $noticeMsg = "Slide show berhasil dihapus.";
	}
}else{
    $notice = true;
    $noticeColor = 'bg-red';
    $noticeMsg = "Slide show gagal dihapus.";
}
?>
