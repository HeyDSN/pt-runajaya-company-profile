<?php
if(isset($_POST['visi'])){
	$id = 1;
	$visi = $_POST['visi'];
	$query = $mysqli->prepare('UPDATE tentang_perusahaan SET visi = ? WHERE id = ?');
	$query->bind_param('ss', $visi, $id);
	if($query->execute()){
	    $notice = true;
	    $noticeColor = 'bg-green';
	    $noticeMsg = "Visi perusahaan berhasil diubah.";
	}else{
	    $notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "Visi perusahaan gagal diubah.";
	}
}
?>
