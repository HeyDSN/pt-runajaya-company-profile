<?php
    $id = $_GET['id'];
    $basefolder = "../files/";
    $filename = $_GET['filename'];
    $filename = $basefolder.$filename;
    // Insert to table and get id
    $query=$mysqli->prepare("DELETE FROM file WHERE id_file=?");
    $query->bind_param('s', $id);
    $query->execute();

    if($query->execute()){
        $notice = true;
	    $noticeColor = 'bg-yellow';
	    $noticeMsg = "file berhasil dihapus.";
    }else{
        $notice = true;
        $noticeColor = 'bg-red';
        $noticeMsg = "file gagal dihapus.";
    }

    if(file_exists($filename)){
    	unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../files/$filename");
    }else{
    	$notice = true;
        $noticeColor = 'bg-red';
        $noticeMsg = "file tidak ditemukan.";
    }
    
?>