<?php
$id = $_GET['id'];
$filename = $_GET['filename'];
// Insert data to table
$query=$mysqli->prepare("DELETE FROM product WHERE id=?");
$query->bind_param('s', $id);
if($query->execute()){
    $affected = $query->affected_rows;
    if($affected >= 1){
        unlink($_SERVER['DOCUMENT_ROOT'].$adminRootFolder."../images/product/$filename");
        $notice = true;
        $noticeColor = 'bg-yellow';
        $noticeMsg = "Product berhasil dihapus.";
    }
}else{
    $notice = true;
    $noticeColor = 'bg-red';
    $noticeMsg = "Product gagal dihapus.";
}
?>
