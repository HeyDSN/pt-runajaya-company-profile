<?php
if(isset($_POST['addUser'])){
	$namaUser = $_POST['namaUser'];
	$emailUser = $_POST['emailUser'];
	$usernameUser = $_POST['usernameUser'];
	
	// Default
    $regFail = false;

	// Check username
    $query1 = $mysqli->prepare('SELECT id_user FROM user WHERE username = ?');
    $query1->bind_param('s', $usernameUser);
    $query1->execute();
    $result1=$query1->get_result();
    $jumlahBaris1=$result1->num_rows;
    if($jumlahBaris1 > 0){
        $regFail = true;
        $regStatMess = "Username";
    }

    // Check email
    $query2 = $mysqli->prepare('SELECT id_user FROM user WHERE email = ?');
    $query2->bind_param('s', $emailUser);
    $query2->execute();
    $result2=$query2->get_result();
    $jumlahBaris2=$result2->num_rows;
    if($jumlahBaris2 > 0){
        $regFail = true;
        if(isset($regStatMess)){
            $regStatMess = "Username & Email";
        }else{
            $regStatMess = "Email";
        }
    }

    if($regFail == false){
		$password = $_POST['passwordUser'];
		$options = [
			'cost' => 10
		];
		$hash = password_hash($password, PASSWORD_BCRYPT, $options);

		// Add user
		$query = $mysqli->prepare('INSERT INTO user (nama, email, username, hash)values(?, ?, ?, ?)');
		$query->bind_param('ssss', $namaUser, $emailUser, $usernameUser, $hash);
		if($query->execute()){
		    $notice = true;
		    $noticeColor = 'bg-green';
		    $noticeMsg = "User [$usernameUser - $namaUser] berhasil ditambah.";
		}else{
		    $notice = true;
		    $noticeColor = 'bg-red';
		    $noticeMsg = "User [$usernameUser - $namaUser] gagal ditambah.";
		}
	}else{
		$notice = true;
	    $noticeColor = 'bg-red';
	    $noticeMsg = "User [$usernameUser - $namaUser] gagal ditambah. Data berikut telah digunakan: $regStatMess";
	}
}
?>
