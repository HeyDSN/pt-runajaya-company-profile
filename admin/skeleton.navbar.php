<body class="hold-transition skin-purple fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../dashboard/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>RJT</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>RJT</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $rootFolder; ?>/images/user-icon.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['nama']; ?></span>
            </a>
            <ul class="dropdown-menu" style="right: 0 !important;">
              <!-- User image -->
              <li class="user-header" style="height: auto !important;">
                <p>
                  <?php echo $_SESSION['nama']; ?> - Administrator
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="../profile/" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="?action=logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGATION</li>
        <li class="<?php if($page=="dashboard"){echo "active";}?>"><a href="../dashboard/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="<?php if($page=="messages"){echo "active";}?>"><a href="../messages/"><i class="fa fa-envelope"></i> <span>Messages</span></a></li>
        <li class="treeview <?php if($page=="about" or $page=="visi-misi" or $page=="struktur"){echo "active";}?>">
          <a href="#">
            <i class="fa fa-briefcase"></i>
            <span>About</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($page=="about"){echo "active";}?>"><a href="../about/"><i class="fa fa-info-circle"></i> <span>Tentang Perusahaan</span></a></li>
            <li class="<?php if($page=="visi-misi"){echo "active";}?>"><a href="../visi-misi/"><i class="fa fa-building"></i> <span>Visi & Misi</span></a></li>
            <li class="<?php if($page=="struktur"){echo "active";}?>"><a href="../struktur/"><i class="fa fa-sitemap"></i> <span>Struktur Perusahaan</span></a></li>
          </ul>
        </li>
        <li class="<?php if($page=="slide-show"){echo "active";}?>"><a href="../slide-show/"><i class="fa fa-th"></i> <span>Slide Show</span></a></li>
        <li class="<?php if($page=="gallery"){echo "active";}?>"><a href="../gallery/"><i class="fa fa-picture-o"></i> <span>Gallery</span></a></li>
        <li class="<?php if($page=="service"){echo "active";}?>"><a href="../service/"><i class="fa fa-tasks"></i> <span>Service</span></a></li>
        <li class="<?php if($page=="product"){echo "active";}?>"><a href="../product/"><i class="fa fa-table"></i> <span>Product</span></a></li>
        <li class="<?php if($page=="testimonials"){echo "active";}?>"><a href="../testimonials/"><i class="fa fa-bullhorn"></i> <span>Testimonials</span></a></li>
        <li class="<?php if($page=="files"){echo "active";}?>"><a href="../files/"><i class="fa fa-file"></i> <span>Files</span></a></li>
        <li class="<?php if($page=="user"){echo "active";}?>"><a href="../user/"><i class="fa fa-user"></i> <span>User</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
