<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 Kopiadem.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- Core JS -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $adminRootFolder; ?>../lte/bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $adminRootFolder; ?>../lte/dist/js/app.min.js"></script>
<script type="text/javascript">
  // Delete Button
  function delBtn(del) {
    var answer = confirm("Are you sure want to Delete?")
    if (answer){
      window.location = del;
    }
  }

  // Fix hide element
  $(function () {
    $("#click").click();
  });

  // Autohide element
  $(function(){
    setTimeout(function(){
    $('#autoHideElement').fadeOut('slow');
  }, 3000); // <-- time in milliseconds
});
</script>

<!-- Optionals JS -->
<?php if($page == "service" or $page == "messages" or $page == "product" or $page == "testimonials" or $page == "user"){ ?>
<!-- DataTables -->
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#dTables").DataTable({
        responsive: true,
        scrollX: true
    });
  });
</script>
<?php } ?>

<?php if($page == "service"){ ?>
<!-- Fa Icon Picker -->
<script src="<?php echo $adminRootFolder; ?>../lte/dist/js/fa-picker.min.js"></script>
<!-- Call plugin -->
<script>
  $(function () {
    $('.icp-auto').iconpicker();
    $('.icp').on('iconpickerSelected', function(e) {
        $('.lead .picker-target').get(0).className = 'picker-target fa-3x ' +
                e.iconpickerInstance.options.iconBaseClass + ' ' +
                e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
    });
  });
</script>
<?php } ?>

<?php if($page == "gallery"){ ?>
<!-- LightGallery -->
<script src="<?php echo $adminRootFolder; ?>../lte/dist/js/lightgallery-all.js"></script>
<!-- Call plugin -->
<script type="text/javascript">
  $(function () {
    $('#aniimated-thumbnials').lightGallery({
        thumbnail: true,
        selector: '.galimg'
    });
  });
</script>
<?php } ?>

<?php if($page == "about" or $page == "visi-misi" or $page == "service" or $page == "product" or $page == "testimonials"){ ?>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo $adminRootFolder; ?>../lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  // Texteditor
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
<?php } ?>

<?php if($page == "slide-show"){ ?>
<script>
var col, el;
$("input[type=radio]").click(function() {
   el = $(this);
   col = el.data("col");
   $("input[data-col=" + col + "]").prop("checked", false);
   el.prop("checked", true);
});
</script>
<?php } ?>
</body>
</html>
