  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Slide Show
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Slide Show</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>

      <?php
      $slideShowHide = false;
      if(isset($_GET['sub-action'])){$subAction = $_GET['sub-action'];}else{$subAction = 'none';}
      if($subAction=='edit-slide'){
        $slideShowHide = true;
        $id = $_GET['id'];
        $db_handle = new DBController();
        $query ="SELECT * FROM slide_show WHERE no='$id'";
        $results = $db_handle->selectQuery($query);
        if($results == 0){
            ?><h3>Gallery not found. <a href="../slide-show/">Back to slide show</a></h3><?php
        }else{
        $no = 0;
        foreach($results as $slide){
      ?>
      <!-- ROW EDIT START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Slide Show</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                <img src="<?php echo $adminRootFolder; ?>/../../images/slide-show/<?php echo $slide['gambar']; ?>" style="max-width:100%"/>
                <br/><br/>
                <form action="?action=edit-slide&id=<?php echo $slide['no']; ?>" method="post">
                <div class="form-group" required>
                  <label for="judulSlide">Judul Slide Show*</label>
                  <input type="text" name="judulSlide" class="form-control" id="judulSlide" placeholder="Judul Slide Show" value="<?php echo $slide['judul']; ?>" required>
                </div>
                <div class="form-group">
                  <label for="deskripsiSlide">Deskripsi Singkat Slide Show</label>
                  <input type="text" name="deskripsiSlide" class="form-control" id="deskripsiSlide" placeholder="Deskripsi Slide Show" value="<?php echo $slide['deskripsi']; ?>">
                </div>
                <div class="form-group">
                  <input type="submit" name="ubahSlide" class="btn btn-primary btn-block" value="Ubah Gambar Slide">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->
      <?php } } } ?>
      
      <?php if($slideShowHide==false){ ?>
      <!-- ROW SATU START -->
      <?php
        $db_handle = new DBController();
        $query ="SELECT COUNT(no) FROM slide_show";
        $jumlahSlide = $db_handle->countQuery($query);
      ?>
      <div class="row" <?php if($jumlahSlide >= $configLimitSlide){echo 'style="display:none;"';} ?>>
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah Slide Show</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form enctype="multipart/form-data" action="?action=add-slide" method="post">
                <div class="form-group">
                  <label for="judulSlide">Judul Slide Show*</label>
                  <input type="text" name="judulSlide" class="form-control" id="judulSlide" placeholder="Judul Slide Show" required>
                </div>
                <div class="form-group">
                  <label for="deskripsiSlide">Deskripsi Singkat Slide Show</label>
                  <input type="text" name="deskripsiSlide" class="form-control" id="deskripsiSlide" placeholder="Deskripsi Slide Show">
                </div>
                <div class="form-group">
                  <label for="gambarSlide">Gambar Slide Show*</label>
                  <input type="file" name="gambarSlide" class="form-control" id="gambarSlide" required>
                </div>
                <div class="form-group">
                  <input type="submit" name="tambahSlide" class="btn btn-primary btn-block" value="Tambal Gambar Slide">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Preview</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                    <?php 
                    $db_handle = new DBController();
                    $query ="SELECT * FROM slide_show ORDER BY urutan ASC";
                    $results = $db_handle->selectQuery($query);
                    if($results == 0){
                        ?><li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li><?php
                    }else{
                    $no = 0;
                    foreach($results as $slide){ ?>
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="<?php if($no == 0){echo 'active';} ?>"></li>
                    <?php $no++; } } ?>

                    </ol>
                    <div class="carousel-inner">

                    <?php 
                    if($results == 0){ ?>
                        <div class="item active">
                        <img src="http://placehold.it/900x500/39CCCC/ffffff&amp;text=I+Love+Bootstrap" alt="First slide">

                        <div class="carousel-caption">
                        First Slide
                        </div>
                    </div>
                    <?php }else{
                    $no = 0;
                    foreach($results as $slide){  ?>
                    <div class="item <?php if($no == 0){echo 'active';} ?>">
                        <img src="<?php echo $adminRootFolder; ?>/../../images/slide-show/<?php echo $slide['gambar']; ?>" alt="<?php echo $slide['judul']; ?>">

                        <div class="carousel-caption">
                        <?php echo $slide['judul']; ?>
                        </div>
                    </div>
                    <?php $no++; } } ?>


                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    </a>
                </div>
                </div>
                <!-- /.box-body -->
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Slide Show List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>#</th>
                  <th>Gambar</th>
                  <th>Urutan</th>
                  <th>Option</th>
                </tr>
                <form action="?action=churutan" method="post">
                <?php 
                if($results == 0){ echo '<tr><td colspan="4" text-align="center">No Data</td></tr>'; }else{
                $no = 0;
                foreach($results as $slide){ $no++ ?>
                <tr>
                  <td style="vertical-align: middle;"><?php echo $no; ?></td>
                  <td style="vertical-align: middle;"><img src="<?php echo $adminRootFolder; ?>/../../images/slide-show/<?php echo $slide['gambar']; ?>" alt="slide-show-<?php echo $no; ?>" title="slide-show-<?php echo $no; ?>" width="150px" height="auto" /></td>
                  <td style="vertical-align: middle;">
                    <?php $noin = 0; foreach($results as $tmp){ $noin++ ?>
                        <input type="radio" name="gambar[<?php echo $no; ?>]" value="<?php echo $slide['no'].'|'.$noin; ?>" data-col="<?php echo $noin; ?>" required> #<?php echo $noin; ?>
                    <?php } ?>
                  </td>
                  <td style="vertical-align: middle;">
                    <a href="?sub-action=edit-slide&id=<?php echo $slide['no']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>&nbsp;
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="delBtn('?action=del-slide&id=<?php echo $slide['no']; ?>&filename=<?php echo $slide['gambar']; ?>')"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php } } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <input type="submit" name="chUrutan" value="Ubah Urutan Slide Show" class="btn btn-primary btn-block" <?php if($results == 0){echo 'disabled style="display:none;"';} ?>>
          </form>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      <?php } ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
