  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Messages
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Messages</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>

      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Daftar Pesan</h3>
                <hr/>
                <table id="dTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM messages";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="6">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $messages){$no++; ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $no; ?></td>
                    <td style="vertical-align: middle;"><?php echo $messages['name']; ?></td>
                    <td style="vertical-align: middle;"><a href="mailto:<?php echo $messages['email']; ?>"><?php echo $messages['email']; ?></a></td>
                    <td style="vertical-align: middle;"><?php echo $messages['subject']; ?></td>
                    <td style="vertical-align: middle;"><?php if($messages['status']==0){echo '<span class="label bg-green">Open</span>'; }else{echo '<span class="label bg-red">Closed</span>'; } ?></td>
                    <td style="vertical-align: middle;">
                      <button class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $no;?>"><i class="fa fa-eye"></i></button>
                      
                      <?php if($messages['status']==0){ ?>
                      <a href="?action=close-messages&id=<?php echo $messages['id']; ?>" class="btn bg-yellow"><i class="fa fa-lock"></i></a>
                      <?php }else{ ?>
                      <a href="javascript:void(0)" style="cursor: not-allowed !important;" title="Disabled" class="btn bg-yellow disabled"><i class="fa fa-lock"></i></a>
                      <?php } ?>

                      <a href="javascript:void(0)" class="btn btn-danger" onclick="delBtn('?action=del-messages&id=<?php echo $messages['id']; ?>')"><i class="fa fa-trash"></i></a>
                      <!-- Modals -->
                      <div class="modal fade" id="modal<?php echo $no;?>" tabindex="-1" role="dialog">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="modal<?php echo $no;?>"><?php echo $messages['subject']; ?></h4>
                                  </div>
                                  <div class="modal-body">
                                      Name: <?php echo $messages['name'].'<br/>'; ?>
                                      Email: <?php echo $messages['email'].'<br/>'; ?>
                                      Phone: <?php echo $messages['phone'].'<br/>'; ?>
                                      Company: <?php echo $messages['company'].'<br/>'; ?>
                                      <br/><p><?php echo $messages['content']; ?></p>
                                  </div>
                                  <div class="modal-footer">     
                                      <button type="button" class="btn btn-primary" data-dismiss="modal">CLOSE</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- /.modals -->
                    </td>
                </tr>
                <?php } } ?>
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
