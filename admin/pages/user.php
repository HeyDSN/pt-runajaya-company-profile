  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      
      <?php
      $userHide = false;
      if(isset($_GET['sub-action'])){$subAction = $_GET['sub-action'];}else{$subAction = 'none';}
      if($subAction=='edit-user'){
        $userHide = true;
        $id = $_GET['id'];
        $db_handle = new DBController();
        $query ="SELECT * FROM user WHERE id_user='$id' AND level = 0";
        $results = $db_handle->selectQuery($query);
        if($results == 0){
            ?><h3>User not found! <a href="../user/">Back to user list</a></h3><?php
        }else{
        $no = 0;
        foreach($results as $user){
      ?>
      <!-- ROW EDIT START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit User Data</h3>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                <form action="?action=edit-user&id=<?php echo $user['id_user']; ?>" method="post">
                <div class="form-group">
                    <label>Nama</label>
                    <input name="namaUser" class="form-control" placeholder="Nama" value="<?php echo $user['nama']; ?>" type="text" required />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="emailUser" class="form-control" placeholder="Email" value="<?php echo $user['email']; ?>" type="text" required />
                    <input name="oldEmailUser" value="<?php echo $user['email']; ?>" type="hidden" />
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input name="usernameUser" class="form-control" placeholder="Uername" value="<?php echo $user['username']; ?>" type="text" required />
                    <input name="oldUsernameUser" value="<?php echo $user['username']; ?>" type="hidden" />
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="passwordUser" class="form-control" placeholder="Password" type="password" required />
                </div>
                <div class="form-group">
                  <input type="submit" name="editUser" value="Edit User Data" class="btn btn-primary btn-block">
                </div>
                </form>
                  <a href="../user/" class="btn btn-danger btn-block">Cancel</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->
      <?php } } } ?>
      
      <?php if($userHide==false){ ?>
      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah User</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form action="?action=add-user" method="post">
                <div class="form-group">
                    <label>Nama</label>
                    <input name="namaUser" class="form-control" placeholder="Nama" type="text" required />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="emailUser" class="form-control" placeholder="Email" type="email" required />
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input name="usernameUser" class="form-control" placeholder="Uername" type="text" required />
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="passwordUser" class="form-control" placeholder="Password" type="password" required />
                </div>
                <div class="form-group">
                  <input type="submit" name="addUser" value="Tambah User" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Daftar User</h3>
                <hr/>
                <table id="dTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM user WHERE level = 0";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="5">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $user){$no++; ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $no; ?></td>
                    <td style="vertical-align: middle;"><?php echo $user['username']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $user['nama']; ?></td>
                    <td style="vertical-align: middle;"><a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a></td>
                    <td style="vertical-align: middle;">
                        <a href="?sub-action=edit-user&id=<?php echo $user['id_user']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>&nbsp;
                        <a href="javascript:void(0)" class="btn btn-danger" onclick="delBtn('?action=del-user&id=<?php echo $user['id_user']; ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } } ?>
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      <?php } ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
