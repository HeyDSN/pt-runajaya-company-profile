  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Testimonials
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Testimonials</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      
      <?php
      $testiHide = false;
      if(isset($_GET['sub-action'])){$subAction = $_GET['sub-action'];}else{$subAction = 'none';}
      if($subAction=='edit-testimonials'){
        $testiHide = true;
        $id = $_GET['id'];
        $db_handle = new DBController();
        $query ="SELECT * FROM testimoni WHERE id_testi='$id'";
        $results = $db_handle->selectQuery($query);
        if($results == 0){
            ?><h3>Not found! <a href="../testimonials/">Back to testimonials list</a></h3><?php
        }else{
        $no = 0;
        foreach($results as $testi){
      ?>
      <!-- ROW EDIT START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Testimonials</h3>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                <form enctype="multipart/form-data" action="?action=edit-testimonials&id=<?php echo $testi['id_testi']; ?>" method="post">
                <div class="form-group">
                  <label for="fotoTesti">Foto</label>
                  <input type="hidden" name="namaFotoTesti" value="<?php echo $testi['foto_testi'] ?>">
                  <input type="file" name="fotoTesti" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input name="namaTesti" class="form-control" placeholder="Nama" value="<?php echo $testi['nama_testi']; ?>" type="text" required />
                </div>
                <div class="form-group">
                    <label>Company</label>
                    <input name="companyTesti" class="form-control" placeholder="Company" value="<?php echo $testi['company_testi']; ?>" type="text" />
                </div>
                <div class="form-group">
                    <label>Testimoni</label>
                    <textarea name="kontenTesti" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?php echo $testi['konten_testi']; ?></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="editTesti" value="Edit Testimonials" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->
      <?php } } } ?>
      
      <?php if($testiHide==false){ ?>
      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah Testimoni</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form enctype="multipart/form-data" action="?action=add-testimonials" method="post">
                <div class="form-group">
                  <label for="fotoTesti">Foto*</label>
                  <input type="file" name="fotoTesti" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama*</label>
                    <input name="namaTesti" class="form-control" placeholder="Nama" type="text" required />
                </div>
                <div class="form-group">
                    <label>Company</label>
                    <input name="companyTesti" class="form-control" placeholder="Company"  type="text" />
                </div>
                <div class="form-group">
                    <label>Testimoni*</label>
                    <textarea name="kontenTesti" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="addTesti" value="Tambah Testimoni" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Daftar Testimoni</h3>
                <hr/>
                <table id="dTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Foto</th>
                  <th>Nama</th>
                  <th>Company</th>
                  <th>Testimoni</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM testimoni";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="6">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $testi){$no++; ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $no; ?></td>
                    <td style="vertical-align: middle;"><img src="<?php echo $adminRootFolder; ?>../images/testi-images/<?php echo $testi['foto_testi']; ?>" alt="<?php echo $testi['foto_testi']; ?>" width="150px" height="auto"></i></td>
                    <td style="vertical-align: middle;"><?php echo $testi['nama_testi']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $testi['company_testi']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $testi['konten_testi']; ?></td>
                    <td style="vertical-align: middle;">
                        <a href="?sub-action=edit-testimonials&id=<?php echo $testi['id_testi']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>&nbsp;
                        <a href="javascript:void(0)" class="btn btn-danger" onclick="delBtn('?action=del-testimonials&id=<?php echo $testi['id_testi']; ?>&filename=<?php echo $testi['foto_testi']; ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } } ?>
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      <?php } ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
