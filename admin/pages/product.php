  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      
      <?php
      $productHide = false;
      if(isset($_GET['sub-action'])){$subAction = $_GET['sub-action'];}else{$subAction = 'none';}
      if($subAction=='edit-product'){
        $productHide = true;
        $id = $_GET['id'];
        $db_handle = new DBController();
        $query ="SELECT * FROM product WHERE id='$id'";
        $results = $db_handle->selectQuery($query);
        if($results == 0){
            ?><h3>Product not found. <a href="../product/">Back to product</a></h3><?php
        }else{
        $no = 0;
        foreach($results as $product){
      ?>
      <!-- ROW EDIT START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Product</h3>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                <form enctype="multipart/form-data" action="?action=edit-product&id=<?php echo $product['id']; ?>" method="post">
                <div class="form-group">
                  <label for="gambarProduct">Product Image</label>
                  <input type="hidden" name="namaGambarProduct" value="<?php echo $product['gambar_product'] ?>">
                  <input type="file" name="gambarProduct" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nama Product</label>
                    <input name="namaProduct" class="form-control" placeholder="Nama Product" value="<?php echo $product['nama_product']; ?>" type="text" required />
                </div>
                <div class="form-group">
                    <label>Deskripsi Product</label>
                    <textarea name="deskripsiProduct" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $product['deskripsi_product']; ?></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="editProduct" value="Edit Product" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->
      <?php } } } ?>
      
      <?php if($productHide==false){ ?>
      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah Product</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form enctype="multipart/form-data" action="?action=add-product" method="post">
                <div class="form-group">
                  <label for="gambarProduct">Product Image*</label>
                  <input type="file" name="gambarProduct" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama Product*</label>
                    <input name="namaProduct" class="form-control" placeholder="Nama Product" type="text" required>
                </div>
                <div class="form-group">
                    <label>Deskripsi Product</label>
                    <textarea name="deskripsiProduct" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="addProduct" value="Tambah Product" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Daftar Product</h3>
                <hr/>
                <table id="dTables" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Product Image</th>
                  <th>Product Name</th>
                  <th>Product Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM product";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="5">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $product){$no++; ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $no; ?></td>
                    <td style="vertical-align: middle;"><img src="<?php echo $adminRootFolder; ?>../images/product/<?php echo $product['gambar_product']; ?>" alt="<?php echo $product['nama_product']; ?>" width="150px" height="auto"></i></td>
                    <td style="vertical-align: middle;"><?php echo $product['nama_product']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $product['deskripsi_product']; ?></td>
                    <td style="vertical-align: middle;">
                        <a href="?sub-action=edit-product&id=<?php echo $product['id']; ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>&nbsp;
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="delBtn('?action=del-product&id=<?php echo $product['id']; ?>&filename=<?php echo $product['gambar_product']; ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } } ?>
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      <?php } ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
