  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <!-- ROW SATU START -->
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Products</span>
                <?php
                $db_handle = new DBController();
                $query ="SELECT COUNT(id) FROM `product`";
                $results = $db_handle->countQuery($query);
                ?>
                <span class="info-box-number"><?php echo $results ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="ion ion-ios-people-outline"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <?php
                $db_handle = new DBController();
                $query ="SELECT COUNT(id_user) FROM `user` WHERE `level` = 0";
                $results = $db_handle->countQuery($query);
                ?>
                <span class="info-box-number"><?php echo $results ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Files</span>
                <?php
                $db_handle = new DBController();
                $query ="SELECT COUNT(id_file) FROM `file`";
                $results = $db_handle->countQuery($query);
                ?>
                <span class="info-box-number"><?php echo $results ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Messages</span>
                <?php
                $db_handle = new DBController();
                $query ="SELECT COUNT(id) FROM `messages`";
                $results = $db_handle->countQuery($query);
                ?>
                <span class="info-box-number"><?php echo $results ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        </div>
      <!-- /.row-one -->

      <!-- ROW DUA START -->
        <div class="row">
          <!-- Kolom produk -->
          <div class="col-md-4">
            <!-- PRODUCT LIST -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Recently Added Products</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="padding-top: 20px; padding-bottom: 20px;">
                <ul class="products-list product-list-in-box">
                  <?php
                  $db_handle = new DBController();
                  $query ="SELECT * FROM product ORDER BY id DESC LIMIT 4";
                  $results = $db_handle->selectQuery($query);
                  if($results !== 0){
                    foreach($results as $product){  ?>
                    <li class="item">
                      <div class="product-img">
                        <img src="<?php echo $adminRootFolder; ?>../images/product/<?php echo $product['gambar_product']; ?>" alt="Product Image">
                      </div>
                      <div class="product-info">
                        <a href="javascript:void(0)" class="product-title"><?php echo $product['nama_product']; ?></a>
                            <span class="product-description">
                              <?php echo $product['deskripsi_product']; ?>
                            </span>
                      </div>
                    </li>
                  <!-- /.item -->
                  <?php } } ?>
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="../product/" class="uppercase">View All Products</a>
              </div>
              <!-- /.box-footer -->
            </div>
          </div>
          <!-- /.col -->

          <!-- Kolom pesan -->
          <div class="col-md-8">
                      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Latest Messages</h3>

              <div class="box-tools">
                <div class="pull-right">
                  <a href="../messages/" class="btn btn-link">View All</a>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="dTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Status</th>
                  <th>View</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM messages ORDER BY id DESC LIMIT 5";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="6">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $messages){$no++; ?>
                <tr>
                  <td style="vertical-align: middle;"><?php echo $no; ?></td>
                  <td style="vertical-align: middle;"><?php echo $messages['name']; ?></td>
                  <td style="vertical-align: middle;"><a href="mailto:<?php echo $messages['email']; ?>"><?php echo $messages['email']; ?></a></td>
                  <td style="vertical-align: middle;"><?php echo $messages['subject']; ?></td>
                  <td style="vertical-align: middle;"><?php if($messages['status']==0){echo '<span class="label bg-green">Open</span>'; }else{echo '<span class="label bg-red">Closed</span>'; } ?></td>
                  <td style="vertical-align: middle;">
                      <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $no;?>"><i class="fa fa-eye"></i></a>
                      <!-- Modals -->
                      <div class="modal fade" id="modal<?php echo $no;?>" tabindex="-1" role="dialog">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="modal<?php echo $no;?>"><?php echo $messages['subject']; ?></h4>
                                  </div>
                                  <div class="modal-body">
                                      Name: <?php echo $messages['name'].'<br/>'; ?>
                                      Email: <?php echo $messages['email'].'<br/>'; ?>
                                      Phone: <?php echo $messages['phone'].'<br/>'; ?>
                                      Company: <?php echo $messages['company'].'<br/>'; ?>
                                      <br/><p><?php echo $messages['content']; ?></p>
                                  </div>
                                  <div class="modal-footer">     
                                      <button type="button" class="btn btn-primary" data-dismiss="modal">CLOSE</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- /.modals -->
                  </td>
              </tr>
              <?php } } ?>
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row-two -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
