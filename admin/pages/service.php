  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Service
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Service</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
      
      <?php
      $serviceHide = false;
      if(isset($_GET['sub-action'])){$subAction = $_GET['sub-action'];}else{$subAction = 'none';}
      if($subAction=='edit-slide'){
        $serviceHide = true;
        $id = $_GET['id'];
        $db_handle = new DBController();
        $query ="SELECT * FROM service WHERE id='$id'";
        $results = $db_handle->selectQuery($query);
        if($results == 0){
            ?><h3>Service not found. <a href="../service/">Back to service</a></h3><?php
        }else{
        $no = 0;
        foreach($results as $service){
      ?>
      <!-- ROW EDIT START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Service</h3>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                <form action="?action=edit-service&id=<?php echo $service['id']; ?>" method="post">
                <div class="form-group">
                    <label>Icon Service*</label>
                    <div class="input-group">
                        <input name="iconService" data-placement="bottomRight" class="form-control icp icp-auto" value="<?php echo $service['icon_service']; ?>" type="text" required />
                        <span class="input-group-addon"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label>Nama Service*</label>
                    <input name="namaService" class="form-control" placeholder="Nama Service" value="<?php echo $service['nama_service']; ?>" type="text" required />
                </div>
                <div class="form-group">
                    <label>Deskripsi Service</label>
                    <textarea name="deskripsiService" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $service['deskripsi_service']; ?></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="editService" value="Edit Service" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->
      <?php } } } ?>
      
      <?php if($serviceHide==false){ ?>
      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah Service</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form action="?action=add-service" method="post">
                <div class="form-group">
                    <label>Icon Service*</label>
                    <div class="input-group">
                        <input name="iconService" data-placement="bottomRight" class="form-control icp icp-auto" value="fa-archive" type="text" required />
                        <span class="input-group-addon"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label>Nama Service*</label>
                    <input name="namaService" class="form-control" placeholder="Nama Service" type="text" required />
                </div>
                <div class="form-group">
                    <label>Deskripsi Service</label>
                    <textarea name="deskripsiService" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="addService" value="Tambah Service" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title">Daftar Service</h3>
              </div><hr>
              <!-- /.box-header -->
                <div class="box-body">
                <table id="dTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Service Icon</th>
                  <th>Service Name</th>
                  <th>Service Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT * FROM service";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ echo '<tr><td colspan="5">No Data</td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td><td style="display: none;"></td></tr>'; }else{
                $no = 0;
                foreach($results as $service){$no++; ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $no; ?></td>
                    <td style="vertical-align: middle;"><i class="fa <?php echo $service['icon_service']; ?> fa-5x"></i></td>
                    <td style="vertical-align: middle;"><?php echo $service['nama_service']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $service['deskripsi_service']; ?></td>
                    <td style="vertical-align: middle;">
                        <a href="?sub-action=edit-slide&id=<?php echo $service['id']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>&nbsp;
                        <a href="javascript:void(0)" class="btn btn-danger" onclick="delBtn('?action=del-service&id=<?php echo $service['id']; ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } } ?>
                </tbody>
                </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      <?php } ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
