    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Struktur Perusahaan
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
          <li class="active">Struktur Perusahaan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>
        <div class="row">
          <div class="col-xs-12">
              <div class="box box-info">
                  <div class="box-header">
                  <h3 class="box-title">Ubah Struktur Persahaan
                  </h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                      <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                      <i class="fa fa-plus"></i></button>
                  </div>
                  <!-- /. tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad" >
                  <form enctype="multipart/form-data" action="?action=edit-struktur" method="post">                  
                  <div class="form-group">
                    <input type="file" name="file" class="form-control" id="file">
                    <?php 
                      $db_handle = new DBController();
                      $query ="SELECT * FROM tentang_perusahaan";
                      $results = $db_handle->selectQuery($query);
                      if($results == 0){}else{
                      $no = 0;
                      foreach($results as $tentang){ $no++ ?>
                      <input type="hidden" name="fileName" value="<?php echo $tentang['struktur']; ?>">
                    <?php } } ?>    
                  </div>
                  <div class="form-group">
                    <input type="submit" name="editStruktur" class="btn btn-primary btn-block" value="Ubah Struktur">
                  </div>
                  </form>
                  </div>
              </div>
              <!-- /.box -->
          </div>
      </div>
      <!-- /.row-->
        <div class="row">
          <div class="col-xs-12">
            <!--box-->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Struktur Perusahaan</h3>
              </div>
              <div class="box-body">
                <?php 
                  $db_handle = new DBController();
                  $query ="SELECT * FROM tentang_perusahaan";
                  $results = $db_handle->selectQuery($query);
                  if($results == 0){}else{
                  $no = 0;
                  foreach($results as $tentag){ $no++ ?>                 
                  <img src="<?php echo $adminRootFolder; ?>../images/<?php echo $tentang['struktur']; ?>" alt="Struktur Perusahaan" style="max-width: 100%; height: auto; display: block; margin: 0 auto;" />
                <?php } } ?>    
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
       
      </section>
      <!-- /.content -->
    </div>
