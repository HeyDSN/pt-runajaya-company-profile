  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tentang Perusahaan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Tentang Perusahaan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->
      
      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>

      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Tentang Perusahaan
                </h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form action="?action=edit-about" method="post">
                <div class="form-group">
                  <textarea name="tentang" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="editAbout" value="Ubah Tentang Perusahaan" class="btn btn-primary btn-block">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

      <!-- ROW DU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tentang Perusahaan
                </h3>
                <hr/>
                <!-- GET ABOUT FROM DB -->
                <?php
                $db_handle = new DBController();
                $query ="SELECT tentang FROM tentang_perusahaan WHERE id = 1";
                $results = $db_handle->selectQuery($query);
                foreach($results as $tentang);
                echo $tentang['tentang'];
                ?>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
