  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Visi & Misi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Visi & Misi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->

      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>

      <!-- ROW SATU START -->
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Edit Visi & Misi Perusahaan
                </h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                
                    <!-- VISI -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                            <h3 class="box-title">Edit Visi Perusahaan
                            </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                            <form action="?action=edit-visi" method="post">
                            <div class="form-group">
                                <textarea name="visi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="editVisi" class="btn btn-primary btn-block">
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>

                    <!-- MISI -->
                    <div class="ccol-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                            <h3 class="box-title">Edit Misi Perusahaan
                            </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                            <form action="?action=edit-misi" method="post">
                            <div class="form-group">
                                <textarea name="misi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="editMisi" class="btn btn-primary btn-block">
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row-->

        <!-- GET VISI MISI FROM DB -->
        <?php
        $db_handle = new DBController();
        $query ="SELECT visi,misi FROM tentang_perusahaan WHERE id = 1";
        $results = $db_handle->selectQuery($query);
        foreach($results as $tentang);
        ?>

      <!-- ROW DU START -->
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Visi Perusahaan
                </h3>
                <hr/>
                
                    <?php echo $tentang['visi']; ?>

                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Misi Perusahaan
                </h3>
                <hr/>
                <?php echo $tentang['misi']; ?>
                </div>
            </div>
            <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
