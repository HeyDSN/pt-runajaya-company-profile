  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gallery
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Gallery</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main content row here -->
      
      <?php if(isset($notice)){ ?>
      <!-- Notification -->
      <div id="autoHideElement" class="alert <?php echo $noticeColor; ?> alert-dismissible" role="alert">
          <?php echo $noticeMsg; ?>
      </div>
      <?php } ?>

      <!-- ROW SATU START -->
        <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                <h3 class="box-title">Tambah Foto ke Gallery</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Open/Close" id="click">
                    <i class="fa fa-plus"></i></button>
                </div>
                <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad" style="display:none;">
                <form enctype="multipart/form-data" action="?action=add-gallery" method="post">
                <div class="form-group">
                    <label for="judulGambar">Judul Foto</label>
                    <input type="text" name="judulGambar" class="form-control" id="judulGambar" placeholder="Judul Foto">
                </div>
                <div class="form-group">
                    <label for="gambarGallery">Foto*</label>
                    <input type="file" name="gambarGallery" class="form-control" id="gambarGallery" required>
                </div>
                <div class="form-group">
                    <input type="submit" name="tambahGallery" class="btn btn-primary btn-block" value="Tambah Foto">
                </div>
                </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        </div>
        <!-- /.row-->

      <!-- ROW DUA START -->
      <div class="row">
        <div class="col-xs-12">
            <div id="aniimated-thumbnials" class="list-unstyled row clearfix" style="text-align: center;">
                <?php 
                $db_handle = new DBController();
                $query ="SELECT * FROM gallery ORDER BY no ASC";
                $results = $db_handle->selectQuery($query);
                if($results == 0){ ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <h3>Gallery Empty!</h3>
                </div>
                <?php }else{
                $no = 0;
                foreach($results as $gallery){ ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <a href="javascript:void(0)" style="color:red;" onclick="delBtn('?action=del-gallery&id=<?php echo $gallery['no']; ?>&filename=<?php echo $gallery['gambar']; ?>')"><i class="fa fa-times"></i> DELETE PHOTO</a>
                    <a href="<?php echo $adminRootFolder; ?>../images/image-gallery/<?php echo $gallery['gambar']; ?>" data-sub-html="<?php echo $gallery['judul']; ?>" class="galimg">
                        <img class="img-responsive thumbnail gallery" src="<?php echo $adminRootFolder; ?>../images/image-gallery/<?php echo $gallery['gambar']; ?>">
                    </a>
                </div>
                <?php } } ?>
            </div>
        </div>
      </div>
      <!-- /.row -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
