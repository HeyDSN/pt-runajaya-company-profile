<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Area | <?php echo ucfirst($page); ?></title>
  
  <!-- Core CSS -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/dist/css/skins/skin-purple.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/plugins/iCheck/flat/blue.css">
  <style>
  .button-container form,
  .button-container form div {
      display: inline;
  }

  .button-container button {
      display: inline;
      vertical-align: middle;
  }
  
  img.gallery{
      object-fit: cover;
      width: 260px;
      height: 146.250px;
      display: block;
      margin: auto;
      margin-bottom: 15px;
  }
  </style>

  <!-- Optionals CSS -->
  <?php if($page == "service" or $page == "messages" or $page == "product" or $page == "testimonials" or $page == "user"){ ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/plugins/datatables/dataTables.bootstrap.css">
  <?php } ?>

  <?php if($page == "gallery"){ ?>
  <!-- Lightgallery Plugin -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/lightgallery/1.3.9/css/lightgallery.css">
  <?php } ?>

  <?php if($page == "service"){ ?>
  <!-- Fa Icon Picker -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/dist/css/fa-picker.min.css">
  <?php } ?>

  <?php if($page == "about" or $page == "visi-misi" or $page == "service" or $page == "product" or $page == "testimonials"){ ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo $adminRootFolder; ?>../lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <?php } ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
