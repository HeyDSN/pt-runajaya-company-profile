<?php
// main config
require_once './includes/autoload.php';

// set default page
if(empty($_GET['page'])){
    header('Location: home');
    die;
}

// save GET to string
$page = $_GET['page'];

if(!file_exists('pages/'.$page.'.php')) {
    //header('Location: ../home');
    include 'pages/error-coy.php';
    die;
}

// header here
include 'skeleton.header.php';

// navbar here
include 'skeleton.navbar.php';

// main content, please using same name as php file
include 'pages/'.$page.'.php';

// footer here
include 'skeleton.footer.php';
?>
