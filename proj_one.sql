-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Agu 2017 pada 10.01
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proj_one`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `file`
--

CREATE TABLE `file` (
  `id_file` int(10) NOT NULL,
  `file` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `upload_date` date NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `file`
--

INSERT INTO `file` (`id_file`, `file`, `file_name`, `upload_date`, `username`) VALUES
(34, '1502698091.xlsx', 'ANALISIS HARIAN 1  KLS 7 SEMESTER GENAP.xlsx', '2017-08-14', 'admin'),
(35, '1502698112.xlsx', 'PROMES VII smt 2.xlsx', '2017-08-14', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE `gallery` (
  `no` int(1) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`no`, `judul`, `gambar`) VALUES
(5, 'foto gede', '1502807363.jpg'),
(6, 'senang senang', '1502807482.jpg'),
(7, 'bersama', '1502807497.jpg'),
(8, 'kawan', '1502807509.jpg'),
(9, 'haha', '1502807519.jpg'),
(10, 'kita semua', '1502814737.jpg'),
(11, 'disini', '1502814759.jpg'),
(14, 'Asus', '1502814908.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `messages`
--

CREATE TABLE `messages` (
  `id` int(5) NOT NULL,
  `name` varchar(70) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL DEFAULT '-',
  `company` varchar(255) NOT NULL DEFAULT '-',
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id` int(5) NOT NULL,
  `gambar_product` varchar(255) NOT NULL,
  `nama_product` varchar(255) NOT NULL,
  `deskripsi_product` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `gambar_product`, `nama_product`, `deskripsi_product`) VALUES
(7, '1502815191.png', 'Product 1', '<p>barang ini di buat di china, spesifikasi :&nbsp;</p><p></p><ul><li>tahan banting</li><li>ukuran 25 cm</li><li>diameter 30 cm</li></ul><p></p>'),
(8, '1502815255.png', 'Product 2', '<p>\r\n\r\n\r\n\r\n</p><p>barang ini di buat di indonesia, spesifikasi : </p><p></p><ul><li>tahan terkena percikan air</li><li>ukuran 25 cm</li><li>diameter 30 cm</li><li>mampu bertahan di kedalam 10 meter</li></ul>\r\n\r\n\r\n\r\n<p></p>'),
(9, '1502815317.png', 'Product 3', '<p>\r\n\r\n\r\n\r\n</p><p>barang ini di buat di jepang, spesifikasi : </p><p></p><ul><li>mudah digunakan</li><li>ukuran 30 cm</li><li>daya tahan yang lama</li></ul>\r\n\r\n\r\n<br><p></p>'),
(10, '1502815334.png', 'Product 4', '<p>\r\n\r\n\r\n\r\n</p><p>barang ini di buat di china, spesifikasi : </p><p></p><ul><li>tahan banting</li><li>ukuran 25 cm</li><li>diameter 30 cm</li></ul>\r\n\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `id` int(5) NOT NULL,
  `icon_service` varchar(255) NOT NULL,
  `nama_service` varchar(255) NOT NULL,
  `deskripsi_service` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`id`, `icon_service`, `nama_service`, `deskripsi_service`) VALUES
(1, 'fa-gears', 'Mekanikal', '<p>instalasi pipa hydrant, pipa compressor, pipa splinkler, pipa vacum, pipa air bersih air kotor dll<br></p>'),
(2, 'fa-bolt', 'Elektrikal', '<p>instalasi listrik lampu, panel-panel, mesin-mesin untuk pabrik , gedung dan apartemen&nbsp;.<br></p>'),
(3, 'fa-plug', 'Elektronik', '<p>Instalasi cctv, access control, barier gate, interlock, catv, matv,  sound system dll<small></small>&nbsp;<br></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide_show`
--

CREATE TABLE `slide_show` (
  `no` int(1) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `urutan` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slide_show`
--

INSERT INTO `slide_show` (`no`, `judul`, `deskripsi`, `gambar`, `urutan`) VALUES
(1, 'I LOVE U', 'we love everybody ', '1502808647.jpg', 2),
(3, 'Need you ', 'we here because of you ', '1502808801.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang_perusahaan`
--

CREATE TABLE `tentang_perusahaan` (
  `id` int(1) NOT NULL,
  `tentang` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `struktur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tentang_perusahaan`
--

INSERT INTO `tentang_perusahaan` (`id`, `tentang`, `visi`, `misi`, `struktur`) VALUES
(1, '<p>\r\n\r\n</p><p>Response Biomedical Corp. develops, manufactures and markets the RAMPÂ® system, a rapid diagnostics platform that delivers lab quality performance for efficient patient management in acute care settings. RAMPÂ® is a global leader in cardiovascular point of care (POC) testing with a wide range of markers available on the platform including Troponin I, CK-MB, Myoglobin, NT-proBNP and D-dimer. For effective and efficient patient care, RAMPÂ® tests aid in the rapid diagnosis of Acute Myocardial Infarction and Heart Failure. RAMPÂ® is ideally suited for both laboratory use and POC testing; POC has the potential to reduce turn around times, increase operational efficiency and improve patient outcomes. Manufactured in a world class facility in Vancouver, Canada, RAMPÂ® tests are of the highest quality and can be run on either the portable, battery-operated, single-port RAMPÂ® reader or the high throughput, modular, RAMPÂ® 200 reader which has additional compliance features and enhanced connectivity. In addition to cardiovascular tests, RAMPÂ® is sold worldwide for use in infectious disease testing, biodefense and environmental applications. The RAMPÂ® system requires no calibration or maintenance, is easy to use, cost-effective and has minimal service requirements. When speed, precision and accuracy is critical, the RAMPÂ® platform provides clinicians the results they need for making informed patient management decisions.</p>\r\n\r\n<br><p></p>', '<p>Towards the implementation of construction and management of construction facilities especially in the field of Electrical by Indonesian labor force, with attention to quality, high reliability and security, and kemamfaaan bbagi human and environment<br></p>', '<p>Making CV. Energy Engineering as a Company Providers of Construction Goods &amp; Services, especially in the field of ELECTRICAL CONTRACTOR which has the National Qualification, Qualification and Classification of Performance.<br></p>', '1502729981.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimoni`
--

CREATE TABLE `testimoni` (
  `id_testi` int(2) NOT NULL,
  `foto_testi` varchar(255) NOT NULL,
  `nama_testi` varchar(70) NOT NULL,
  `company_testi` varchar(255) NOT NULL,
  `konten_testi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `testimoni`
--

INSERT INTO `testimoni` (`id_testi`, `foto_testi`, `nama_testi`, `company_testi`, `konten_testi`) VALUES
(2, '1502813662.png', 'Bachtiar RIvaie', 'Kopiadem', '<p>good website</p>'),
(3, '1502813715.png', 'Andimala Salomo', 'Kopiadem', '<p>best service</p>'),
(4, '1502815051.png', 'Jaka Purnama', 'Kopiadem', '<p>\r\n\r\nsangat memuaskan semua service yang telah diberikan\r\n\r\n<br></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(25) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `level` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `hash`, `level`) VALUES
(1, 'Super Admin', 'admin@admin.com', 'admin', '$2y$10$JXa8B7Akk//qNiu6sv1GlO0WHH88ZsamN4u8OEMBLmJeY8aJIU1lW', '1'),
(3, 'Deni Setiawan', 'deni.1743@gmail.com', 'dsniwa', '$2y$10$kqicvh2u2z1YleYjPFfO.OcaGSj/bbkL3TTRzPpuQBYa.14Y0DrHO', '0'),
(4, 'Ade Yusup Permana', 'adeyusupp@gmail.com', 'yusup', '$2y$10$VQHWbRsuEJTispA8/Yhacu4./ObQ9esNYcLszssVh4MwnqydKmuiu', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_show`
--
ALTER TABLE `slide_show`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tentang_perusahaan`
--
ALTER TABLE `tentang_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id_testi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id_file` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `no` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `slide_show`
--
ALTER TABLE `slide_show`
  MODIFY `no` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tentang_perusahaan`
--
ALTER TABLE `tentang_perusahaan`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id_testi` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
