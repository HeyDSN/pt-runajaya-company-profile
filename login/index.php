<?php // main config
require_once '../includes/autoload.php';

if (isset($_SESSION['lvl'])) {
  $lvl = $_SESSION['lvl'];
  if ($lvl==1) {
    header("Location: $rootFolder/admin");
  }else{
    header("Location: $rootFolder/user");
  }
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RTJ | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $userRootFolder; ?>../lte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $userRootFolder; ?>../lte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $userRootFolder; ?>../lte/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Runa Jaya Telindotama</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
        <?php if (isset($_POST['sign-in'])){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $query1 = $mysqli->prepare('SELECT * FROM user WHERE username = ?');
            $query1->bind_param('s', $username);
            $query1->execute();
            $result1=$query1->get_result();
            $jumlahBaris1=$result1->num_rows;
            if($jumlahBaris1 > 0){
                $row=$result1->fetch_array();
                $hash=$row['hash'];
                $lvl=$row['level'];
                $nama=$row['nama'];
                $verif=password_verify($password, $hash);
                  if ($verif==1) {
                    if ($lvl==1) {
                      $_SESSION['lvl']=1;
                      $_SESSION['username']=$username;
                      $_SESSION['nama']=$nama;
                      header("Location: ../admin");
                    }else{
                      $_SESSION['lvl']=0;
                      $_SESSION['username']=$username;
                      $_SESSION['nama']=$nama;
                      header("Location: ../user");
                    }
                  }else{
                    echo '
                      <div id="autoHideElement" class="alert bg-red alert-dismissible" role="alert">
                         Wrong Password.
                      </div>';
                  }
            }else{
              echo '
                <div id="autoHideElement" class="alert bg-red alert-dismissible" role="alert">
                  Login Failed.
                </div>';
            }
          }
         ?>
    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <input type="submit" name="sign-in" class="btn btn-primary btn-block btn-flat" value="Sign In">
        </div>
        <!-- /.col -->
      </div>
    </form>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo $userRootFolder; ?>../lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $userRootFolder; ?>../lte/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
  // Autohide element
  $(function(){
    setTimeout(function(){
    $('#autoHideElement').fadeOut('slow');
  }, 3000); // <-- time in milliseconds
});
</script>
</body>
</html>
