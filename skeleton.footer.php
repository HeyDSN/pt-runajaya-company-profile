   <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
   <!--<section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Tentang Perusahaan</h3>
                        <ul>
                            <li><a href="#">Company Profile</a></li>
                            <li><a href="#">Visi & Misi</a></li>
                            <li><a href="#">Struktur Perusahaan</a></li>
                        </ul>
                    </div>    
                </div><!~~/.col-md-3~~>

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Layanan Perusahaan</h3>
                        <ul>
                            <li><a href="#">Service 1</a></li>
                            <li><a href="#">Service 2</a></li>
                            <li><a href="#">Service 3</a></li>
                        </ul>
                    </div>    
                </div><!~~/.col-md-3~~>

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Partner/Sponsor</h3>
                        <ul>
                            <li><a href="#">Partner 1</a></li>
                            <li><a href="#">Partner 2</a></li>
                            <li><a href="#">Partner 3</a></li>
                            <li><a href="#">Partner 4</a></li>
                        </ul>
                    </div>    
                </div><!~~/.col-md-3~~>

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <a href="#"><h3>Kontak Kami</h3></a>
                        <ul>
                            
                        </ul>
                    </div>    
                </div><!~~/.col-md-3~~>
            </div>
        </div>
    </section>--><!--/#bottom-->
    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <div class="col-sm-3 col-md-3">
                    <div style="margin-top: 30px;">
                    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9825081063154!2d107.07060311505779!3d-6.266030395464642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698fbc1e11a327%3A0xa9cd3cab9bb1941c!2sMetland+Tambun!5e0!3m2!1sid!2sid!4v1502866849096"></iframe>        
                    </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                    <div class="alamat">
                        <address>
                            <h5 style="color: #FFFFFF;"><strong>ALAMAT KANTOR</strong></h5>
                            <p>PT. RUNA JAYA TELINDOTAMA <br><br>
                            Metland Tambun<br>
                            Jl.Pirus V Blok J1 No.30 <br>
                            Tambun - Bekasi <br><br>
                            </p>                                    
                        </address>
                    </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <address>
                            <h5 style="color: #FFFFFF;"><strong>JAM BUKA </strong></h5>
                            <p>Senin-jum'at : Jam 8:00 - 14:30 WIB<br><br>
                            Phone: +62 21 88 333 3100 <br>
                            Faxmile : +62 21 88 333 100 <br>
                            Email: ptrunajaya@gmail.com <br></p>                              
                        </address>
                    </div><div class="col-sm-3 col-md-3">
                       <h5 style="color: #FFFFFF;"><strong>MENU</strong></h5>
                       <a href="../home/">Home</a><br>
                       <a href="../tentang/">Tentang</a><br>
                       <a href="../layanan/">Layanan</a><br>
                       <a href="../produk/">Produk</a><br>
                       <a href="../galeri/">Galeri</a><br>
                       <a href="../kontak/">Kontak</a><br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <center>&copy; 2017 Kopiadem. All Rights Reserved.</center>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="<?php echo $rootFolder; ?>/dist/js/jquery.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/jquery.isotope.min.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/main.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/wow.min.js"></script>
    <script src="<?php echo $rootFolder; ?>/dist/js/owl.carousel.js"></script>

    <?php if($page == "galeri" or $page == "produk"){ ?>
    <!-- LightGallery -->
    <script src="<?php echo $adminRootFolder; ?>../lte/dist/js/lightgallery-all.js"></script>
    <!-- Call plugin -->
    <script type="text/javascript">
      $(function () {
        $('#aniimated-thumbnials').lightGallery({
            thumbnail: true,
            selector: '.galimg'
        });
      });
    </script>
    <?php } ?>

    <script type="text/javascript">
    $(function () {
        $('.owl-carousel').owlCarousel({
            autoplay: true,
            autoplayHoverPause:false,
            loop:true,
            dots: false,
            autoWidth: true,
            smartSpeed: 500,
            margin: 50
        });
    });

    $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
    });

    </script>
</body>
</html>